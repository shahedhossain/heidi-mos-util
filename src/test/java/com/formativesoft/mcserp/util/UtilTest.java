package com.formativesoft.mcserp.util;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class UtilTest extends TestCase {
	
	public static final String METADATA     = "{\"implicitIncludes\":true, \"messageProperty\":\"message\", \"successProperty\":\"success\"}";
	public static final String MESSAGE_TPL	= "{\"metaData\":%s, \"success\":%s, \"message\":\"%s\"}";

	public UtilTest(String testName) {
		super(testName);
	}

	public static Test suite() {
		return new TestSuite(UtilTest.class);
	}

	public void testFirst() {
		assertTrue(true);
	}
	
	public void testSecond() {
		assertTrue(true);
	}
	
	public void testMore() {
		System.out.println(String.format(MESSAGE_TPL, METADATA, true, "Success"));
		System.out.println(String.format(MESSAGE_TPL, METADATA, false, "Failure"));
		assertTrue(true);
	}
}
