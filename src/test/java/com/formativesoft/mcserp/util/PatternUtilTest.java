package com.formativesoft.mcserp.util;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import biz.shahed.freelance.heidi.mos.util.PatternUtil;

public class PatternUtilTest extends TestCase {

	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(PatternUtilTest.class);

	public PatternUtilTest(String testName) {
		super(testName);
	}

	public static Test suite() {
		return new TestSuite(PatternUtilTest.class);
	}

	public void testIsMatch() {
		assertTrue(PatternUtil.isMatch("m00i001", "m[\\d]{2}[i|s|t]{1}[\\d]{3}"));
		assertTrue(PatternUtil.isMatch("m00s001", "m[\\d]{2}[i|s|t]{1}[\\d]{3}"));
		assertTrue(PatternUtil.isMatch("m00t001", "m[\\d]{2}[i|s|t]{1}[\\d]{3}"));
		assertFalse(PatternUtil.isMatch("m00j001", "m[\\d]{2}[i|s|t]{1}[\\d]{3}"));
		assertFalse(PatternUtil.isMatch("m00k001", "m[\\d]{2}[i|s|t]{1}[\\d]{3}"));
		assertTrue(PatternUtil.isMatch("M00001", "[M|m]{1}[\\d]{5}"));		
	}
	
	public void testGroovy(){
		assertFalse(PatternUtil.isMatch("META-INF/m00001/m00i001/groovy/", "META-INF/m[\\d]{2}001/m[\\d]{2}[b|i|q|r|s|t]{1}001/groovy/[\\w\\-\\/\\.]++"));
		assertTrue(PatternUtil.isMatch("META-INF/m00001/m00i001/groovy/Calculator.groovy", "META-INF/m[\\d]{2}001/m[\\d]{2}[b|i|q|r|s|t]{1}001/groovy/[\\w\\-\\/\\.]*+"));
		assertTrue(PatternUtil.isMatch("META-INF/m00001/m00i001/groovy/Calculator.groovy", "META-INF/m[\\d]{2}001/m[\\d]{2}[b|i|q|r|s|t]{1}001/groovy/[\\w\\-\\/\\.]++"));
	}
	
	public void testJscript(){
		assertFalse(PatternUtil.isMatch("META-INF/m00001/js/", "META-INF/m[\\d]{2}001/js/[\\w\\-\\/\\.]++"));
		assertTrue(PatternUtil.isMatch("META-INF/m00001/js/Viewport.js", "META-INF/m[\\d]{2}001/js/[\\w\\-\\/\\.]++"));
		assertTrue(PatternUtil.isMatch("META-INF/m00001/js/Viewport.js", "META-INF/m[\\d]{2}001/js/[\\w\\-\\/\\.]*+"));
	}
	
	public void testSpring(){
		assertFalse(PatternUtil.isMatch("META-INF/m00001/spring/", "META-INF/m[\\d]{2}001/spring/[\\w\\-\\/\\.]++"));
		assertTrue(PatternUtil.isMatch("META-INF/m00001/spring/m00i001/", "META-INF/m[\\d]{2}001/spring/[\\w\\-\\/\\.]++"));
		assertTrue(PatternUtil.isMatch("META-INF/m00001/spring/m00i001/applicationContext-m00i001-task.xml", "META-INF/m[\\d]{2}001/spring/[\\w\\-\\/\\.]*+"));
	}
	
	public void testW3Script(){
		assertFalse(PatternUtil.isMatch("META-INF/m00001/m00i001/w3script/", "META-INF/m[\\d]{2}001/m[\\d]{2}[b|i|q|r|s|t]{1}001/w3script/[\\w\\-\\/\\.]++"));
		assertTrue(PatternUtil.isMatch("META-INF/m00001/m00i001/w3script/controller/C00I001001.js", "META-INF/m[\\d]{2}001/m[\\d]{2}[b|i|q|r|s|t]{1}001/w3script/[\\w\\-\\/\\.]*+"));
		assertTrue(PatternUtil.isMatch("META-INF/m00001/m00i001/w3script/controller/C00I001001.js", "META-INF/m[\\d]{2}001/m[\\d]{2}[b|i|q|r|s|t]{1}001/w3script/[\\w\\-\\/\\.]++"));
	}
	
	public void testW3ScriptController(){
		assertTrue(PatternUtil.isMatch("META-INF/m00001/m00i001/w3script/controller/", "META-INF/m[\\d]{2}001/m[\\d]{2}[b|i|q|r|s|t]{1}001/w3script/controller/"));
		assertTrue(PatternUtil.isMatch("META-INF/m00001/m00i001/w3script/controller/C00I001001.js", "META-INF/m[\\d]{2}001/m[\\d]{2}[b|i|q|r|s|t]{1}001/w3script/controller/[\\w\\-\\/\\.]*+"));
		assertTrue(PatternUtil.isMatch("META-INF/m00001/m00i001/w3script/controller/C00I001001.js", "META-INF/m[\\d]{2}001/m[\\d]{2}[b|i|q|r|s|t]{1}001/w3script/controller/C[\\d]{2}[B|I|Q|R|S|T]{1}[\\d]{6}.js"));
	}
	
	public void testW3ScriptData(){
		assertTrue(PatternUtil.isMatch("META-INF/m00001/m00i001/w3script/data/", "META-INF/m[\\d]{2}001/m[\\d]{2}[b|i|q|r|s|t]{1}001/w3script/data/"));
		assertTrue(PatternUtil.isMatch("META-INF/m00001/m00i001/w3script/data/D00I001001.js", "META-INF/m[\\d]{2}001/m[\\d]{2}[b|i|q|r|s|t]{1}001/w3script/data/[\\w\\-\\/\\.]*+"));
		assertTrue(PatternUtil.isMatch("META-INF/m00001/m00i001/w3script/data/D00I001001.js", "META-INF/m[\\d]{2}001/m[\\d]{2}[b|i|q|r|s|t]{1}001/w3script/data/D[\\d]{2}[B|I|Q|R|S|T]{1}[\\d]{6}.js"));
	}
	
	public void testW3ScriptModel(){
		assertTrue(PatternUtil.isMatch("META-INF/m00001/m00i001/w3script/model/", "META-INF/m[\\d]{2}001/m[\\d]{2}[b|i|q|r|s|t]{1}001/w3script/model/"));
		assertTrue(PatternUtil.isMatch("META-INF/m00001/m00i001/w3script/model/M00I001001.js", "META-INF/m[\\d]{2}001/m[\\d]{2}[b|i|q|r|s|t]{1}001/w3script/model/[\\w\\-\\/\\.]*+"));
		assertTrue(PatternUtil.isMatch("META-INF/m00001/m00i001/w3script/model/M00I001001.js", "META-INF/m[\\d]{2}001/m[\\d]{2}[b|i|q|r|s|t]{1}001/w3script/model/M[\\d]{2}[B|I|Q|R|S|T]{1}[\\d]{6}.js"));
	}
	
	public void testW3ScriptStore(){
		assertTrue(PatternUtil.isMatch("META-INF/m00001/m00i001/w3script/store/", "META-INF/m[\\d]{2}001/m[\\d]{2}[b|i|q|r|s|t]{1}001/w3script/store/"));
		assertTrue(PatternUtil.isMatch("META-INF/m00001/m00i001/w3script/store/S00I001001.js", "META-INF/m[\\d]{2}001/m[\\d]{2}[b|i|q|r|s|t]{1}001/w3script/store/[\\w\\-\\/\\.]*+"));
		assertTrue(PatternUtil.isMatch("META-INF/m00001/m00i001/w3script/store/S00I001001.js", "META-INF/m[\\d]{2}001/m[\\d]{2}[b|i|q|r|s|t]{1}001/w3script/store/S[\\d]{2}[B|I|Q|R|S|T]{1}[\\d]{6}.js"));
	}
	
	public void testW3ScriptView(){
		assertTrue(PatternUtil.isMatch("META-INF/m00001/m00i001/w3script/view/", "META-INF/m[\\d]{2}001/m[\\d]{2}[b|i|q|r|s|t]{1}001/w3script/view/"));
		assertTrue(PatternUtil.isMatch("META-INF/m00001/m00i001/w3script/view/V00I001001.js", "META-INF/m[\\d]{2}001/m[\\d]{2}[b|i|q|r|s|t]{1}001/w3script/view/[\\w\\-\\/\\.]*+"));
		assertTrue(PatternUtil.isMatch("META-INF/m00001/m00i001/w3script/view/V00I001001.js", "META-INF/m[\\d]{2}001/m[\\d]{2}[b|i|q|r|s|t]{1}001/w3script/view/V[\\d]{2}[B|I|Q|R|S|T]{1}[\\d]{6}.js"));
	}
	
	public void testW3ScriptViewport(){
		assertTrue(PatternUtil.isMatch("META-INF/m00001/m00i001/w3script/view/", "META-INF/m[\\d]{2}001/m[\\d]{2}[b|i|q|r|s|t]{1}001/w3script/view/"));
		assertTrue(PatternUtil.isMatch("META-INF/m00001/m00i001/w3script/view/V00I001001.js", "META-INF/m[\\d]{2}001/m[\\d]{2}[b|i|q|r|s|t]{1}001/w3script/view/((V[\\d]{2}[B|I|Q|R|S|T]{1}[\\d]{6})|(Viewport)).js"));
		assertTrue(PatternUtil.isMatch("META-INF/m00001/m00i001/w3script/view/Viewport.js", "META-INF/m[\\d]{2}001/m[\\d]{2}[b|i|q|r|s|t]{1}001/w3script/view/((V[\\d]{2}[B|I|Q|R|S|T]{1}[\\d]{6})|(Viewport)).js"));
	}
	
	public void testW3ScriptSubview(){
		assertTrue(PatternUtil.isMatch("META-INF/m00001/m00i001/w3script/view/v00i001001/", "META-INF/m[\\d]{2}001/m[\\d]{2}[b|i|q|r|s|t]{1}001/w3script/view/v[\\d]{2}[b|i|q|r|s|t]{1}[\\d]{6}/"));
		assertTrue(PatternUtil.isMatch("META-INF/m00001/m00i001/w3script/view/v00i001001/V00I001001X01.js", "META-INF/m[\\d]{2}001/m[\\d]{2}[b|i|q|r|s|t]{1}001/w3script/view/v[\\d]{2}[b|i|q|r|s|t]{1}[\\d]{6}/V[\\d]{2}[B|I|Q|R|S|T]{1}[\\d]{6}X[\\d]{2}.js"));
		assertFalse(PatternUtil.isMatch("META-INF/m00001/m00i001/w3script/view/v00k001001/V00I001001X01.js", "META-INF/m[\\d]{2}001/m[\\d]{2}[b|i|q|r|s|t]{1}001/w3script/view/v[\\d]{2}[b|i|q|r|s|t]{1}[\\d]{6}/V[\\d]{2}[B|I|Q|R|S|T]{1}[\\d]{6}X[\\d]{2}.js"));
	}
	
	public void testReference(){
		assertFalse(PatternUtil.isMatch("t", "[a-z]{1}[\\w]*+.[a-z]{1}[\\w]*+"));
		assertFalse(PatternUtil.isMatch("t.", "[a-z]{1}[\\w]*+.[a-z]{1}[\\w]*+"));
		assertFalse(PatternUtil.isMatch("t._", "[a-z]{1}[\\w]*+.[a-z]{1}[\\w]*+"));
		assertTrue(PatternUtil.isMatch("t.id", "[a-z]{1}[\\w]*+.[a-z]{1}[\\w]*+"));
		assertTrue(PatternUtil.isMatch("t01t001.id", "[a-z]{1}[\\w]*+.[a-z]{1}[\\w]*+"));
		assertTrue(PatternUtil.isMatch("t01i001.name", "[a-z]{1}[\\w]*+.[a-z]{1}[\\w]*+"));
		assertTrue(PatternUtil.isMatch("shAhed.shahed", "[a-z]{1}[\\w]*+.[a-z]{1}[\\w]*+"));
		assertFalse(PatternUtil.isMatch("shAhed.shahed.x", "[a-z]{1}[\\w]*+.[a-z]{1}[\\w]*+"));
	}
	
	public void testValidProperty(){
		assertTrue(PatternUtil.isMatch("x", "[a-z]{1}[\\w&&[^\\_]]*+"));
		assertTrue(PatternUtil.isMatch("m01i001", "[a-z]{1}[\\w&&[^\\_]]*+"));
		assertTrue(PatternUtil.isMatch("name", "[a-z]{1}[\\w&&[^\\_]]*+"));
		assertTrue(PatternUtil.isMatch("nameEn", "[a-z]{1}[\\w&&[^\\_]]*+"));
		assertTrue(PatternUtil.isMatch("yourNameIs", "[a-z]{1}[\\w&&[^\\_]]*+"));
		
		assertFalse(PatternUtil.isMatch("x.", "[a-z]{1}[\\w&&[^\\_]]*+"));
		assertFalse(PatternUtil.isMatch("01i001", "[a-z]{1}[\\w&&[^\\_]]*+"));
		assertFalse(PatternUtil.isMatch("name_", "[a-z]{1}[\\w&&[^\\_]]*+"));
		assertFalse(PatternUtil.isMatch("_nameEn", "[a-z]{1}[\\w&&[^\\_]]*+"));
		assertFalse(PatternUtil.isMatch("yourNameIs-", "[a-z]{1}[\\w&&[^\\_]]*+"));
	}
	
	public void testAuthority(){
		String regex = "^ROLE_C[\\d]{2}[B|I|J|Q|R|S|T|X]{1}[\\d]{6}_(([S|B]{1}[C|R|U|D]{1})|JR)[\\d]{3}$";
		
		assertTrue(PatternUtil.isMatch("ROLE_C01I001001_SR001", regex));
		assertTrue(PatternUtil.isMatch("ROLE_C01I001001_SR002", regex));
		assertTrue(PatternUtil.isMatch("ROLE_C01I001001_SC001", regex));
		assertTrue(PatternUtil.isMatch("ROLE_C01I001001_SC002", regex));
		assertTrue(PatternUtil.isMatch("ROLE_C01I001001_SU001", regex));
		assertTrue(PatternUtil.isMatch("ROLE_C01I001001_SU002", regex));
		assertTrue(PatternUtil.isMatch("ROLE_C01I001001_SD001", regex));
		assertTrue(PatternUtil.isMatch("ROLE_C01I001001_SD002", regex));
		
		assertTrue(PatternUtil.isMatch("ROLE_C01I001001_BR001", regex));
		assertTrue(PatternUtil.isMatch("ROLE_C01I001001_BR002", regex));		
		assertTrue(PatternUtil.isMatch("ROLE_C01I001001_BC001", regex));
		assertTrue(PatternUtil.isMatch("ROLE_C01I001001_BC002", regex));
		assertTrue(PatternUtil.isMatch("ROLE_C01I001001_BU001", regex));
		assertTrue(PatternUtil.isMatch("ROLE_C01I001001_BU002", regex));
		assertTrue(PatternUtil.isMatch("ROLE_C01I001001_BD001", regex));
		assertTrue(PatternUtil.isMatch("ROLE_C01I001001_BD002", regex));
		
		assertTrue(PatternUtil.isMatch("ROLE_C01I001001_JR001", regex));
		assertTrue(PatternUtil.isMatch("ROLE_C01I001001_JR002", regex));
	}

}
