package com.formativesoft.mcserp.util;

public class D00I000 {
	public Boolean wrapperBoolean;
	public boolean primitiveBoolean;
	
	public Byte wrapperByte;
	public byte primitiveByte;
	
	public Short wrapperShort;
	public short primitiveShort;
	
	public Character wrapperChar;
	public char primitiveChar;
	
	public Integer wrapperInt;
	public int primitiveInt;
	
	public Long wrapperLong;
	public long primitiveLong;
	
	public Float wrapperFloat;
	public float primitivefloat;	
	
	public Double wrapperDouble;
	public double primitiveDouble;
	
	public String stringType;
	
	public D00I000 noJavaType;
}