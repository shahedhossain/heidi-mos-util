package com.formativesoft.mcserp.util;

import java.lang.reflect.Field;

import biz.shahed.freelance.heidi.mos.util.TypeUtil;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class TypeUtilTest extends TestCase {

	public TypeUtilTest(String testName) {
		super(testName);
	}

	public static Test suite() {
		return new TestSuite(TypeUtilTest.class);
	}

	public void testBoolean() {
		Class<?> clazz = D00I000.class;
		for (Field field : clazz.getDeclaredFields()) {
			if (field.getName().equals("wrapperBoolean")) {
				assertTrue(TypeUtil.isWrapperBoolean(field));
				assertFalse(TypeUtil.isPrimitiveBoolean(field));				
			} else if (field.getName().equals("primitiveBoolean")) {
				assertTrue(TypeUtil.isPrimitiveBoolean(field));
				assertFalse(TypeUtil.isWrapperBoolean(field));
			}
		}
	}

	public void testByte() {
		Class<?> clazz = D00I000.class;
		for (Field field : clazz.getDeclaredFields()) {
			if (field.getName().equals("wrapperByte")) {
				assertTrue(TypeUtil.isWrapperInt(field));
				assertFalse(TypeUtil.isPrimitiveInt(field));				
			} else if (field.getName().equals("primitiveByte")) {
				assertTrue(TypeUtil.isPrimitiveInt(field));
				assertFalse(TypeUtil.isWrapperInt(field));
			}
		}
	}
	
	public void testShort() {
		Class<?> clazz = D00I000.class;
		for (Field field : clazz.getDeclaredFields()) {
			if (field.getName().equals("wrapperShort")) {
				assertTrue(TypeUtil.isWrapperInt(field));
				assertFalse(TypeUtil.isPrimitiveInt(field));				
			} else if (field.getName().equals("primitiveShort")) {
				assertTrue(TypeUtil.isPrimitiveInt(field));
				assertFalse(TypeUtil.isWrapperInt(field));
			}
		}
	}

	public void testChar() {
		Class<?> clazz = D00I000.class;
		for (Field field : clazz.getDeclaredFields()) {
			if (field.getName().equals("wrapperChar")) {
				assertTrue(TypeUtil.isString(field));
				assertTrue(TypeUtil.isWrapperChar(field));				
				assertFalse(TypeUtil.isPrimitiveChar(field));				
			} else if (field.getName().equals("primitiveChar")) {
				assertTrue(TypeUtil.isString(field));
				assertTrue(TypeUtil.isPrimitiveChar(field));
				assertFalse(TypeUtil.isWrapperInt(field));
			}
		}
	}

	public void testInt() {
		Class<?> clazz = D00I000.class;
		for (Field field : clazz.getDeclaredFields()) {
			if (field.getName().equals("wrapperInt")) {
				assertTrue(TypeUtil.isWrapperInt(field));
				assertFalse(TypeUtil.isPrimitiveInt(field));				
			} else if (field.getName().equals("primitiveInt")) {
				assertTrue(TypeUtil.isPrimitiveInt(field));
				assertFalse(TypeUtil.isWrapperInt(field));
			}
		}
	}

	public void testLong() {
		Class<?> clazz = D00I000.class;
		for (Field field : clazz.getDeclaredFields()) {
			if (field.getName().equals("wrapperLong")) {
				assertTrue(TypeUtil.isWrapperInt(field));
				assertFalse(TypeUtil.isPrimitiveInt(field));				
			} else if (field.getName().equals("primitiveLong")) {
				assertTrue(TypeUtil.isPrimitiveInt(field));
				assertFalse(TypeUtil.isWrapperInt(field));
			}
		}
	}

	public void testFloat() {
		Class<?> clazz = D00I000.class;
		for (Field field : clazz.getDeclaredFields()) {
			if (field.getName().equals("wrapperFloat")) {
				assertTrue(TypeUtil.isWrapperFloat(field));
				assertFalse(TypeUtil.isPrimitiveFloat(field));				
			} else if (field.getName().equals("primitivefloat")) {
				assertTrue(TypeUtil.isPrimitiveFloat(field));
				assertFalse(TypeUtil.isWrapperFloat(field));
			}
		}
	}

	public void testDouble() {
		Class<?> clazz = D00I000.class;
		for (Field field : clazz.getDeclaredFields()) {
			if (field.getName().equals("wrapperDouble")) {
				assertTrue(TypeUtil.isWrapperFloat(field));
				assertFalse(TypeUtil.isPrimitiveFloat(field));				
			} else if (field.getName().equals("primitiveDouble")) {
				assertTrue(TypeUtil.isPrimitiveFloat(field));
				assertFalse(TypeUtil.isWrapperFloat(field));
			}
		}
	}
	
	public void testString() {
		Class<?> clazz = D00I000.class;
		for (Field field : clazz.getDeclaredFields()) {
			if (field.getName().equals("stringType")) {
				assertFalse(TypeUtil.isWrapper(field));
				assertFalse(TypeUtil.isPrimitive(field));				
				assertFalse(TypeUtil.isPrimitiveInt(field));
				assertFalse(TypeUtil.isWrapperInt(field));
				assertTrue(TypeUtil.isString(field));
			}
		}
	}
	
	public void testNoJavaType() {
		Class<?> clazz = D00I000.class;
		for (Field field : clazz.getDeclaredFields()) {
			if (field.getName().equals("noJavaType")) {
				assertFalse(TypeUtil.isWrapper(field));
				assertFalse(TypeUtil.isPrimitive(field));				
				assertFalse(TypeUtil.isPrimitiveInt(field));
				assertFalse(TypeUtil.isWrapperInt(field));
				assertFalse(TypeUtil.isJavaType(field));
			}
		}
	}
	
}
