package com.formativesoft.mcserp.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import biz.shahed.freelance.heidi.mos.util.FreeMarkerUtil;

import freemarker.template.Configuration;

public class FreeMarkerTest extends TestCase {

	private static final Logger log = LoggerFactory
			.getLogger(FreeMarkerTest.class);

	public FreeMarkerTest(String testName) {
		super(testName);
	}

	public static Test suite() {
		return new TestSuite(FreeMarkerTest.class);
	}

	public void testFirst() {
		Map<String, Object> root = new HashMap<String, Object>();
		String tpl = "Hello ${country}";
		root.put("country", "Bangladesh!");
		String expected = FreeMarkerUtil.parse(tpl, root);
		String actual  	= "Hello Bangladesh!";
		assertEquals(expected, actual);
	}

	public void testSecond() {
		Map<String, Object> root = new HashMap<String, Object>();
		String tpl = "Hello ${division}";
		root.put("division", "Dhaka!");
		String expected = FreeMarkerUtil.parse(tpl, root);
		String actual  	= "Hello Dhaka!";
		assertEquals(expected, actual);
	}

	public void testMore() {
		Map<String, Object> root = new HashMap<String, Object>();
		String tpl = "Hello ${user}";
		root.put("user", "Shahed!");
		String expected = FreeMarkerUtil.parse(tpl, root);
		String actual  	= "Hello Shahed!";
		assertEquals(expected, actual);
	}
	
	public void testAndMore() {
		String tpl = "Hello Shahed!";
		String expected = FreeMarkerUtil.parse(tpl);
		String actual  	= "Hello Shahed!";
		assertEquals(expected, actual);
	}
	
	public void testFtlFile() {
		Map<String, Object> root = new HashMap<String, Object>();		
		List<MediaFile> list = new ArrayList<MediaFile>();
		list.add(new MediaFile("shahed"));
		list.add(new MediaFile("Abdul"));
		root.put("list", list);
		
		Configuration cfg = FreeMarkerUtil.getConfig(FreeMarkerTest.class, "/META-INF/util/js/");
		String value = FreeMarkerUtil.parse(cfg, "Fsl.ftl", root);
		log.info(value);
		
		assertTrue(true);
	}
	
	/*public void testJScript() {
		String jsFile = "util/b/c/Date";
		assertEquals(W3JScriptImpl.resolveClassName(jsFile), "Date");
		assertEquals(W3JScriptImpl.resolvePackageName(jsFile), "Fsl.util.b.c");
		assertEquals(W3JScriptImpl.getJScriptFile(jsFile), "util/b/c/Date.js");
	}*/
	
}
