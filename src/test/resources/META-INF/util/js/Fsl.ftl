<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
 
 <html> 
  <head>
  	<title>hello</title>
  </head> 
 <body>
  <div align="center">
    <table width="80%">
<#assign maxItembyRow = 2>
<#assign index = 0>
<#assign count = 0>
<#assign newLine = true>
<#foreach mediaFile in list>
  <#if newLine>
      <tr>
  <#assign newLine = false>
  </#if>
        <td><a href="${mediaFile.name}" id="player${count}"></a></td>
  <#if index < maxItembyRow-1>        
    <#assign index = index + 1>
  <#else>
    <#assign index = 0>
    <#assign newLine = true>
  </#if>
  <#if newLine>
      </tr>
  </#if>
  <#assign count = count + 1>
</#foreach>
  </table>
  </div>
  </body>
</html>