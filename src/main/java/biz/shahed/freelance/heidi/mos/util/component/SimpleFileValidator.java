package biz.shahed.freelance.heidi.mos.util.component;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.multipart.MultipartFile;

import biz.shahed.freelance.heidi.mos.util.upload.SingleFileUpload;

@Component
public class SimpleFileValidator implements Validator {

	private static final Logger log = LoggerFactory.getLogger(SimpleFileValidator.class);

	private static final String[] ACCEPTED_CONTENT_TYPES = new String[] {
			"application/pdf",
			"application/doc",
			"application/msword",
			"application/rtf",
			"text/richtext",
			"text/rtf",
			"text/plain",
			"application/vnd.openxmlformats-officedocument.wordprocessingml.document",
			"application/vnd.sun.xml.writer", "application/x-soffice" };

	private static final String[] ACCEPTED_EXTENSIONS = new String[] { "doc",
			"pdf", "docx", "rtf", "txt" };

	@Override
	public boolean supports(Class<?> clazz) {
		return SingleFileUpload.class.equals(clazz);
	}

	@Override
	public void validate(Object obj, Errors errors) {
		SingleFileUpload uploadItem = (SingleFileUpload) obj;
		MultipartFile file = uploadItem.getFile();
		try {
			if (file == null || file.getBytes().length == 0) {
				errors.reject("error.upload.null", "File name can't be empty");
				return;
			}
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}

		boolean acceptableContentType = false;
		String incomingContentType = file.getContentType();

		if ("application/octet-stream".equalsIgnoreCase(incomingContentType)) {
			int index = file.getOriginalFilename().lastIndexOf('.');
			String incomingExtension = file.getOriginalFilename().substring(index + 1);
			for (String extendsion : ACCEPTED_EXTENSIONS) {
				if (extendsion.equalsIgnoreCase(incomingExtension)) {
					acceptableContentType = true;
					break;
				}
			}
		} else {
			for (String contentType : ACCEPTED_CONTENT_TYPES) {
				if (contentType.equalsIgnoreCase(incomingContentType)) {
					acceptableContentType = true;
					break;
				}
			}
		}
		
		if (!acceptableContentType) {
			errors.reject(
					"error.upload.contenttype",
					"Please upload a file with one of the following file types; .doc, .docx, .txt, .pdf, .rtf .");
		}
	}

}
