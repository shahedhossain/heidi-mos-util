package biz.shahed.freelance.heidi.mos.util;

import javax.servlet.http.HttpServletRequest;

import nl.bitwalker.useragentutils.Browser;
import nl.bitwalker.useragentutils.UserAgent;

public class UserAgentUtil {
	
	public static UserAgent getUserAgent(HttpServletRequest request){
		String userAgent = request.getHeader("User-Agent");
		return new UserAgent(userAgent);
	}
	
	public static boolean isMsie(HttpServletRequest request){
		UserAgent agent = getUserAgent(request);
		Browser browser = agent.getBrowser();
		return browser.getGroup() == Browser.IE;
	}
	
	public static boolean isFirefox(HttpServletRequest request){
		UserAgent agent = getUserAgent(request);
		Browser browser = agent.getBrowser();
		return browser.getGroup() == Browser.FIREFOX;
	}

}
