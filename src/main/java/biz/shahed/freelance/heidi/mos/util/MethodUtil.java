package biz.shahed.freelance.heidi.mos.util;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MethodUtil {

	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(MethodUtil.class);
	
	public static final String BOOL_GETTER_PREFIX 	= "is";
	public static final String GETTER_PREFIX 		= "get";
	public static final String SETTER_PREFIX 		= "set";
	
	public static boolean isBoolean(Method method) {
		return ClassUtil.isBoolean(method.getReturnType());
	}

	public static boolean isBoolean(Method method, boolean isPrimitive) {
		return ClassUtil.isBoolean(method.getReturnType(), isPrimitive);
	}
		
	public static boolean isBooleanPrimitive(Method method) {
		return ClassUtil.isBooleanPrimitive(method.getReturnType());
	}

	public static boolean isBooleanWrapper(Method method) {
		return ClassUtil.isBooleanWrapper(method.getReturnType());
	}

	public static boolean isByte(Method method) {
		return ClassUtil.isByte(method.getReturnType());
	}

	public static boolean isByte(Method method, boolean isPrimitive) {
		return ClassUtil.isByte(method.getReturnType(), isPrimitive);
	}

	public static boolean isBytePrimitive(Method method) {
		return ClassUtil.isBytePrimitive(method.getReturnType());
	}

	public static boolean isByteWrapper(Method method) {
		return ClassUtil.isByteWrapper(method.getReturnType());
	}

	public static boolean isCharacter(Method method) {
		return ClassUtil.isCharacter(method.getReturnType());
	}
	
	public static boolean isCharacter(Method method, boolean isPrimitive) {
		return ClassUtil.isCharacter(method.getReturnType(), isPrimitive);
	}

	public static boolean isCharacterPrimitive(Method method) {
		return ClassUtil.isCharacterPrimitive(method.getReturnType());
	}

	public static boolean isCharacterWrapper(Method method) {
		return ClassUtil.isCharacterWrapper(method.getReturnType());
	}

	public static boolean isCollection(Method method) {
		return ClassUtil.isCollection(method.getReturnType());
	}

	public static boolean isDouble(Method method) {
		return ClassUtil.isDouble(method.getReturnType());
	}

	public static boolean isDouble(Method method, boolean isPrimitive) {
		return ClassUtil.isDouble(method.getReturnType(), isPrimitive);
	}

	public static boolean isDoublePrimitive(Method method) {
		return ClassUtil.isDoublePrimitive(method.getReturnType());
	}

	public static boolean isDoubleWrapper(Method method) {
		return ClassUtil.isDoubleWrapper(method.getReturnType());
	}

	public static boolean isFloat(Method method) {
		return ClassUtil.isFloat(method.getReturnType());
	}
	
	public static boolean isFloat(Method method, boolean isPrimitive) {
		return ClassUtil.isFloat(method.getReturnType(), isPrimitive);
	}
	
	public static boolean isFloatPrimitive(Method method) {
		return ClassUtil.isFloatPrimitive(method.getReturnType());
	}

	public static boolean isFloatWrapper(Method method) {
		return ClassUtil.isFloatWrapper(method.getReturnType());
	}

	public static boolean isGetter(Method method) {
		String methodName = method.getName();
		if (methodName.startsWith(GETTER_PREFIX)) {
			if (Character.isUpperCase(methodName.charAt(3))) {
				if (isPublic(method) && method.getParameterTypes().length == 0) {
					if (!isVoid(method)) {
						return true;
					}
				}
			}
		} else if (methodName.startsWith(BOOL_GETTER_PREFIX)) {
			if (Character.isUpperCase(methodName.charAt(2))) {
				if (isPublic(method) && method.getParameterTypes().length == 0) {
					if (isBoolean(method)) {
						return true;
					}
				}
			}
		}
		return false;
	}

	public static boolean isInteger(Method method) {
		return ClassUtil.isInteger(method.getReturnType());
	}

	public static boolean isInteger(Method method, boolean isPrimitive) {
		return ClassUtil.isInteger(method.getReturnType(), isPrimitive);
	}

	public static boolean isIntegerPrimitive(Method method) {
		return ClassUtil.isIntegerPrimitive(method.getReturnType());
	}

	public static boolean isIntegerWrapper(Method method) {
		return ClassUtil.isIntegerWrapper(method.getReturnType());
	}

	public static boolean isList(Method method) {
		return ClassUtil.isList(method.getReturnType());
	}

	public static boolean isLong(Method method) {
		return ClassUtil.isLong(method.getReturnType());
	}

	public static boolean isLong(Method method, boolean isPrimitive) {
		return ClassUtil.isLong(method.getReturnType(), isPrimitive);
	}

	public static boolean isLongPrimitive(Method method) {
		return ClassUtil.isLongPrimitive(method.getReturnType());
	}

	public static boolean isLongWrapper(Method method) {
		return ClassUtil.isLongWrapper(method.getReturnType());
	}

	public static boolean isMap(Method method) {
		return ClassUtil.isMap(method.getReturnType());
	}

	public static boolean isPrimitive(Method method) {
		return ClassUtil.isPrimitive(method.getReturnType());
	}

	public static boolean isPrivate(Method method){
		return Modifier.isPrivate(method.getModifiers());
	}
	
	public static boolean isProtected(Method method){
		return Modifier.isProtected(method.getModifiers());
	}

	public static boolean isPublic(Method method){
		return Modifier.isPublic(method.getModifiers());
	}
	
	public static boolean isSet(Method method) {
		return ClassUtil.isSet(method.getReturnType());
	}
	
	public static boolean isSetter(Method method) {
		String methodName = method.getName();
		if (methodName.startsWith(SETTER_PREFIX)) {
			if (Character.isUpperCase(methodName.charAt(3))) {
				if (isPublic(method) && method.getParameterTypes().length == 1) {
					if (isVoid(method)) {
						return true;
					}
				}
			}
		}
		return false;
	}
	
	public static boolean isShort(Method method) {
		return ClassUtil.isShort(method.getReturnType());
	}

	public static boolean isShort(Method method, boolean isPrimitive) {
		return ClassUtil.isShort(method.getReturnType(), isPrimitive);
	}

	public static boolean isShortPrimitive(Method method) {
		return ClassUtil.isShortPrimitive(method.getReturnType());
	}
	
	public static boolean isShortWrapper(Method method) {
		return ClassUtil.isShortWrapper(method.getReturnType());
	}
	
	public static boolean isStatic(Method method){
		return Modifier.isStatic(method.getModifiers());
	}
	
	public static boolean isString(Method method) {
		return ClassUtil.isString(method.getReturnType());
	}
	
	public static boolean isVoid(Method method) {
		return ClassUtil.isVoid(method.getReturnType());
	}
	
	public static boolean isWrapper(Method method) {
		return ClassUtil.isWrapper(method.getReturnType());
	}
	
}
