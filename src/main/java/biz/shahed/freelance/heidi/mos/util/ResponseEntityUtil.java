package biz.shahed.freelance.heidi.mos.util;

import java.util.Date;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ResponseEntityUtil {

	public static ResponseEntity<String> getCssResponseEntity(String css) {
		HttpHeaders headers = HeaderUtil.getHttpHeadersForCSS();
		return getResponseEntity(css, headers);
	}

	public static ResponseEntity<String> getCssResponseEntity(String css, Date date, int maxCacheAgeInSeccond) {
		HttpHeaders headers = HeaderUtil.getHttpHeadersForCSS(date, maxCacheAgeInSeccond);
		return getResponseEntity(css, headers);
	}

	public static ResponseEntity<String> getCssResponseEntity(String css, int maxCacheAgeInSeccond) {
		HttpHeaders headers = HeaderUtil.getHttpHeadersForCSS(maxCacheAgeInSeccond);
		return getResponseEntity(css, headers);
	}

	public static ResponseEntity<String> getHtmlResponseEntity(String html) {
		HttpHeaders headers = HeaderUtil.getHttpHeadersForHtml();
		return getResponseEntity(html, headers);
	}

	public static ResponseEntity<String> getHtmlResponseEntity(String html, Date date, int maxCacheAgeInSeccond) {
		HttpHeaders headers = HeaderUtil.getHttpHeadersForHtml(date, maxCacheAgeInSeccond);
		return getResponseEntity(html, headers);
	}
		
	public static ResponseEntity<String> getHtmlResponseEntity(String html, int maxCacheAgeInSeccond) {
		HttpHeaders headers = HeaderUtil.getHttpHeadersForHtml(maxCacheAgeInSeccond);
		return getResponseEntity(html, headers);
	}

	public static ResponseEntity<String> getJavaScriptResponseEntity(String js) {
		HttpHeaders headers = HeaderUtil.getHttpHeadersForJS();
		return getResponseEntity(js, headers);
	}

	public static ResponseEntity<String> getJavaScriptResponseEntity(String js, Date date, int maxCacheAgeInSeccond) {
		HttpHeaders headers = HeaderUtil.getHttpHeadersForJS(date, maxCacheAgeInSeccond);
		return getResponseEntity(js, headers);
	}
	
	public static ResponseEntity<String> getJavaScriptResponseEntity(String js, int maxCacheAgeInSeccond) {
		HttpHeaders headers = HeaderUtil.getHttpHeadersForJS(maxCacheAgeInSeccond);
		return getResponseEntity(js, headers);
	}

	public static ResponseEntity<String> getJsonResponseEntity(String json) {
		HttpHeaders headers = HeaderUtil.getHttpHeadersForJSON();
		return getResponseEntity(json, headers);
	}
	
	public static ResponseEntity<String> getJsonResponseEntity(String json, Date date, int maxCacheAgeInSeccond) {
		HttpHeaders headers = HeaderUtil.getHttpHeadersForJSON(date, maxCacheAgeInSeccond);
		return getResponseEntity(json, headers);
	}
	
	public static ResponseEntity<String> getJsonResponseEntity(String json, int maxCacheAgeInSeccond) {
		HttpHeaders headers = HeaderUtil.getHttpHeadersForJSON(maxCacheAgeInSeccond);
		return getResponseEntity(json, headers);
	}

	public static ResponseEntity<String> getRedirectResponseEntity(String actionPath) {
		HttpHeaders headers = new HttpHeaders();
		headers.add("Location", actionPath);
		return new ResponseEntity<String>(null, headers, HttpStatus.MOVED_TEMPORARILY);
	}

	public static ResponseEntity<String> getResponseEntity(String body, Date date, int maxCacheAgeInSeccond, String contentType) {
		HttpHeaders headers = HeaderUtil.getHttpHeaders(date, maxCacheAgeInSeccond, contentType);
		return getResponseEntity(body, headers);
	}
	
	public static ResponseEntity<String> getResponseEntity(String body, HttpHeaders headers) {
		return getResponseEntity(body, headers, HttpStatus.OK);
	}

	public static ResponseEntity<String> getResponseEntity(String body, HttpHeaders headers, HttpStatus statusCode) {
		return new ResponseEntity<String>(body, headers, statusCode);
	}

	public static ResponseEntity<String> getResponseEntity(String body, int maxCacheAgeInSeccond, String contentType) {
		HttpHeaders headers = HeaderUtil.getHttpHeaders(maxCacheAgeInSeccond, contentType);
		return getResponseEntity(body, headers);
	}
	
	public static ResponseEntity<String> getResponseEntity(String body, String contentType) {
		HttpHeaders headers = HeaderUtil.getHttpHeaders(contentType);
		return getResponseEntity(body, headers);
	}

}
