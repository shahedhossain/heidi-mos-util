package biz.shahed.freelance.heidi.mos.util.env;

public class HeidiMosImpl implements HeidiMos{

	private String environment = DEVELOPMENT;

	@Override
	public String getEnvironment() {
		return environment;
	}

	public void setEnvironment(String environment) {
		this.environment = environment;
	}

	@Override
	public boolean isDevelopment() {
		return environment.equals(DEVELOPMENT);
	}

	@Override
	public boolean isProduction() {
		return environment.equals(PRODUCTION);
	}

	@Override
	public boolean isDebug() {
		return environment.equals(DEBUG);
	}
	
}
