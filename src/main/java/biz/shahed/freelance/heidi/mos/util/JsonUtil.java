package biz.shahed.freelance.heidi.mos.util;

import java.text.SimpleDateFormat;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig.Feature;
import org.codehaus.jackson.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JsonUtil {

	private static final Logger log = LoggerFactory.getLogger(JsonUtil.class);
	
	public static final String SENCHA_DATE_FORMATE = "yyyy-MM-dd'T'HH:mm:ss";

	public static String objectAsString(Object object) {
		String html = "";
		try {
			ObjectMapper mapper = getObjectMapper();
			html = mapper.writeValueAsString(object);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return html;
	}
	
	public static Object stringAsObject(String string, TypeReference<?> type ) {
		ObjectMapper mapper = getObjectMapper();
		try {
			return mapper.readValue(string, type);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return null;
	}
	
	public static  ObjectMapper getObjectMapper(){
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(Feature.WRITE_DATES_AS_TIMESTAMPS, false);
		mapper.setDateFormat(new SimpleDateFormat(SENCHA_DATE_FORMATE));
		return mapper;
	}

}
