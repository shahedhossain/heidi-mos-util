package biz.shahed.freelance.heidi.mos.util.date;

import java.util.Date;

import biz.shahed.freelance.heidi.mos.util.DateUtil;

public class Age {

	public Age(Date birthDate) {
		
		long sysTime 	= (new Date()).getTime();
		long birthTime = birthDate.getTime();
		
		long ageMillis	= sysTime - birthTime;
		process(ageMillis);
	}
	
	public Age(int years, int months) {
		
		float days = years * DateUtil.DAYS_PER_YEAR + months* DateUtil.DAYS_PER_MONTH;
		double ageMillis  = days * DateUtil.HOURS_PER_DAY;
		
		ageMillis *= DateUtil.MINUTES_PER_HOUR;
		ageMillis *= DateUtil.SECONDS_PER_MINUTE;
		ageMillis *= DateUtil.MILLISECONDS_PER_SECOND;
		
		process((long) ageMillis);
	}
	
	public Age(int years, int months, int days) {
		
		float ageDays = years * DateUtil.DAYS_PER_YEAR + months* DateUtil.DAYS_PER_MONTH + days;
		double ageMillis  = ageDays * DateUtil.HOURS_PER_DAY;
		
		ageMillis *= DateUtil.MINUTES_PER_HOUR;
		ageMillis *= DateUtil.SECONDS_PER_MINUTE;
		ageMillis *= DateUtil.MILLISECONDS_PER_SECOND;
		
		process((long) ageMillis);
	}	
	
	public void process(long ageMillis) {

		this.ageMillis = ageMillis;
		
		ageSeconds	= (double)ageMillis / (double)DateUtil.MILLISECONDS_PER_SECOND ;
		ageMinutes	= ageSeconds / DateUtil.SECONDS_PER_MINUTE;
		ageHours   	= ageMinutes / DateUtil.MINUTES_PER_HOUR;
		ageDays     = (float) (ageHours / DateUtil.HOURS_PER_DAY);
		ageMonths  	= ageDays / DateUtil.DAYS_PER_MONTH;
		ageYears   	= ageDays / DateUtil.DAYS_PER_YEAR;
		
		years 	= (int) Math.floor(ageYears);
		months	= (int) Math.floor(getPoint(ageYears) * DateUtil.MONTHS_PER_YEAR);
		days	= (int) Math.floor(getPoint(ageMonths) * DateUtil.DAYS_PER_MONTH);
		hours	= (int) Math.floor(getPoint(ageDays) * DateUtil.HOURS_PER_DAY);
		minutes	= (long) Math.floor(getPoint(ageHours) * DateUtil.MINUTES_PER_HOUR);
		seconds	= (long) Math.floor(getPoint(ageMinutes) * DateUtil.SECONDS_PER_MINUTE);
	}
	
	private double getPoint(double number) {
		return number - Math.floor(number);
	}
	
	public Date getBirithDate() {
		long sysTime 	= (new Date()).getTime();
		return new Date(sysTime - ageMillis);
	}
	
	public  float getAgeYears() {
		return ageYears;
	}
	
	public  float getAgeMonths() {
		return ageMonths;
	}
	
	public  float getAgeDays() {
		return ageDays;			
	}
	
	public  double getAgeHours() {
		return ageHours;
	}
	
	public  double getAgeMinutes() {
		return ageMinutes;
	}
	
	public  double getAgeSeconds() {
		return ageSeconds;
	}
	
	public  long getAgeMillis() {
		return ageMillis;
	}
	
	public  int getYears() {
		return years;
	}
	
	public  int getMonths() {
		return months;
	}
	
	public  int getDays() {
		return days;
	}
	
	public  int getHours() {
		return hours;
	}
	
	public  long getMinutes() {
		return minutes;
	}
	
	public  long getSeconds() {
		return seconds;
	}
			
	private long ageMillis;
	private double ageSeconds;
	private double ageMinutes;
	private double ageHours;
	private float ageDays;
	private float ageMonths;
	private float ageYears;
	
	private int years;
	private int months;
	private int days;
	private int hours;
	private long minutes;
	private long seconds;
}
