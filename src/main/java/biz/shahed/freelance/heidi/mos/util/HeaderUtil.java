package biz.shahed.freelance.heidi.mos.util;

import java.util.Date;

import org.springframework.http.HttpHeaders;

public class HeaderUtil {
	
	public static final String CACHE_CONTROL = "Cache-Control";
	public static final String CACHE_CONTROL_EXP = "max-age=%s, must-revalidate";
	public static final String CONTENT_TYPE = "Content-Type";
	public static final String CONTENT_TYPE_EXP = "%s; charset=UTF-8";
	public static final String EXPIRES = "Expires";
	
	public static final String LAST_MODIFIED = "Last-Modified";
	public static final String JS_CONTENT_TYPE = "text/javascript";
	public static final String CSS_CONTENT_TYPE = "text/css";
	public static final String JSON_CONTENT_TYPE = "application/json";
	public static final String HTML_CONTENT_TYPE = "text/html";
	
	public static final int DEFAULT_MAX_AGE_IN_SECOND = 60;	

	public static String getExpriesDateTimeInGMTString(Date date, int maxCacheAgeInSeccond) {
		long time = date.getTime();
		long mage = maxCacheAgeInSeccond * 1000;
		Date expi = new Date(time + mage);
		return DateUtil.toGmtString(expi);
	}
	
	public static String getExpriesDateTimeInGMTString(int maxCacheAgeInSeccond) {
		Date date = new Date();
		return getExpriesDateTimeInGMTString(date, maxCacheAgeInSeccond);
	}
	
	public static HttpHeaders getHttpHeaders(Date date, int maxCacheAgeInSeccond, String contentType){
		HttpHeaders headers = new HttpHeaders();
		headers.add(CACHE_CONTROL, String.format(CACHE_CONTROL_EXP, maxCacheAgeInSeccond));
		headers.add(CONTENT_TYPE, String.format(CONTENT_TYPE_EXP, contentType));
		headers.add(EXPIRES, getExpriesDateTimeInGMTString(date, maxCacheAgeInSeccond));
		headers.add(LAST_MODIFIED, getLastModifiedDateTimeInGMTString(date));
		return headers;
	}

	public static HttpHeaders getHttpHeaders(int maxCacheAgeInSeccond, String contentType){
		Date date = new Date();
		return getHttpHeaders(date, maxCacheAgeInSeccond, contentType);
	}
	
	public static HttpHeaders getHttpHeaders(String contentType){
		return getHttpHeaders(DEFAULT_MAX_AGE_IN_SECOND, contentType);
	}
		
	public static HttpHeaders getHttpHeadersForCSS(){
		return getHttpHeadersForCSS(DEFAULT_MAX_AGE_IN_SECOND);
	}
	
	public static HttpHeaders getHttpHeadersForCSS(Date date, int maxCacheAgeInSeccond){
		return getHttpHeaders(date, maxCacheAgeInSeccond, CSS_CONTENT_TYPE);
	}
	
	public static HttpHeaders getHttpHeadersForCSS(int maxCacheAgeInSeccond){
		return getHttpHeadersForCSS(new Date(), maxCacheAgeInSeccond);
	}

	public static HttpHeaders getHttpHeadersForHtml(){
		return getHttpHeadersForHtml(DEFAULT_MAX_AGE_IN_SECOND);
	}
	
	public static HttpHeaders getHttpHeadersForHtml(Date date, int maxCacheAgeInSeccond){
		return getHttpHeaders(date, maxCacheAgeInSeccond, HTML_CONTENT_TYPE);
	}
	
	public static HttpHeaders getHttpHeadersForHtml(int maxCacheAgeInSeccond){
		return getHttpHeadersForHtml(new Date(), maxCacheAgeInSeccond);
	}
	
	public static HttpHeaders getHttpHeadersForJS(){
		return getHttpHeadersForJS(DEFAULT_MAX_AGE_IN_SECOND);
	}
	
	public static HttpHeaders getHttpHeadersForJS(Date date, int maxCacheAgeInSeccond){
		return getHttpHeaders(date, maxCacheAgeInSeccond, JS_CONTENT_TYPE);
	}
	
	public static HttpHeaders getHttpHeadersForJS(int maxCacheAgeInSeccond){
		return getHttpHeadersForJS(new Date(), maxCacheAgeInSeccond);
	}
	
	public static HttpHeaders getHttpHeadersForJSON(){
		return getHttpHeadersForJSON(DEFAULT_MAX_AGE_IN_SECOND);
	}
	
	public static HttpHeaders getHttpHeadersForJSON(Date date, int maxCacheAgeInSeccond){
		return getHttpHeaders(date, maxCacheAgeInSeccond, JSON_CONTENT_TYPE);
	}
	
	public static HttpHeaders getHttpHeadersForJSON(int maxCacheAgeInSeccond){
		return getHttpHeadersForJSON(new Date(), maxCacheAgeInSeccond);
	}
	
	public static String getLastModifiedDateTimeInGMTString() {
		Date date = new Date();
		return getLastModifiedDateTimeInGMTString(date);
	}
	
	public static String getLastModifiedDateTimeInGMTString(Date date) {
		return DateUtil.toGmtString(date);
	}
	
}
