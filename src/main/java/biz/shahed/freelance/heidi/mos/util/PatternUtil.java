package biz.shahed.freelance.heidi.mos.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatternUtil {
	
	public static boolean isMatch(String input, String regex){
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(input);
		return matcher.matches();
	}
	
	public static boolean isMatchIgnoreCase(String input, String regex){
		Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(input);
		return matcher.matches();
	}

}
