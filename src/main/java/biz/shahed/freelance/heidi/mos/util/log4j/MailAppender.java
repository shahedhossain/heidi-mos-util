package biz.shahed.freelance.heidi.mos.util.log4j;

import java.util.Date;

import org.apache.log4j.net.SMTPAppender;

import biz.shahed.freelance.heidi.mos.util.DateUtil;

public class MailAppender extends SMTPAppender {

	@Override
	public void setSubject(String subject) {
		String date = DateUtil.format(new Date(), "MMM dd, yyyy HH:mm:ss a");
		subject = String.format("%s [%s]", subject, date.toUpperCase());
		super.setSubject(subject);
	}

}
