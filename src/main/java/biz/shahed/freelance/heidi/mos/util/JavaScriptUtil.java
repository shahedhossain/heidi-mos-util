package biz.shahed.freelance.heidi.mos.util;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

import org.mozilla.javascript.EvaluatorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import biz.shahed.freelance.heidi.mos.util.javascript.JavaScriptErrorReporter;

import com.yahoo.platform.yui.compressor.JavaScriptCompressor;

public class JavaScriptUtil {

	private static Logger log = LoggerFactory.getLogger(JavaScriptUtil.class);

	private static void minifyJS(StringReader reader, StringWriter writer) throws EvaluatorException, IOException {
		JavaScriptErrorReporter reporter = new JavaScriptErrorReporter();
		JavaScriptCompressor compressor = new JavaScriptCompressor(reader, reporter);
		compressor.compress(writer, -1, true, false, true, false);
	}

	public static String minifyJS(String js) {
		StringReader reader = new StringReader(js);
		StringWriter writer = new StringWriter();
		try {
			minifyJS(reader, writer);
		} catch (EvaluatorException e) {
			log.error(e.getMessage(), e);
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}
		return writer.toString();
	}
}
