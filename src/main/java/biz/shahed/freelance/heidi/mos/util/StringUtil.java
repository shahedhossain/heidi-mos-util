package biz.shahed.freelance.heidi.mos.util;

import java.util.Date;

public class StringUtil {

	private static final String ENTITY_CLASS_PREFIX = "com.rdpgroupbd.erp.orm.entity.";
	private static final String ENTITY_CLASS_SUBFIX = "m[0-9]{5}\\p{Punct}T[0-9]{2}[B|I|T][0-9]{3}";

	public static String getEntityId(String toString) {
		String identity = "";
		if (isValidEntity(toString)) {
			int beginIndex = toString.lastIndexOf("=") + 1;
			identity = toString.substring(beginIndex).replace("]", "");
		}
		return identity;
	}

	public static boolean getEntityIdAsBool(String toString) {
		return BooleanUtil.parseBool(getEntityId(toString));
	}

	public static Date getEntityIdAsDate(String toString) {
		String date = getEntityId(toString);
		return DateUtil.parse(date, DateUtil.YYYYMMDD);
	}

	public static int getEntityIdAsInt(String toString) {
		return NumberUtil.parseInt(getEntityId(toString));
	}

	public static String getEntityName(String toString) {
		String name = null;
		if (toString != null && !toString.equals("")) {
			if (toString.indexOf("=") != -1) {
				int beginIndex = 0;
				int endIndex = toString.lastIndexOf("[");

				if (endIndex > -1) {
					name = toString.substring(beginIndex, endIndex);
				}
			}
		}

		return name;
	}

	public static int getHashCode(Double value) {
		return value == null ? 0 : value.hashCode();
	}
	
	public static int getHashCode(Float value) {
		return value == null ? 0 : value.hashCode();
	}
	
	public static int getHashCode(Integer value) {
		return value == null ? 0 : value.hashCode();
	}
	
	public static int getHashCode(String value) {
		return value == null ? 0 : value.hashCode();
	}

	public static String initCap(String input) {
		if(input != null && input.length() >= 2){
			String firtChar = (input.charAt(0) + "").toUpperCase();
			String lastStrn = input.substring(1);
			input = String.format("%s%s", firtChar, lastStrn);
		}
		return input;
	}

	public static boolean isEquals(Object self, Object other) {
		if (self == null && other != null) {
			return false;
		} else if (self != null && other == null) {
			return false;
		} else if (self != null && other != null) {
			return self.equals(other);
		}
		return true;
	}
	
	public static boolean isValidEntity(String toString) {
		boolean validFlag = false;
		String canonicalName = getEntityName(toString);
		if (canonicalName != null && !canonicalName.equals(""))
			if (canonicalName.startsWith(ENTITY_CLASS_PREFIX)) {
				String array[] = canonicalName.split(ENTITY_CLASS_PREFIX);
				String subfix = array.length == 2 ? array[1] : "";
				validFlag = subfix.matches(ENTITY_CLASS_SUBFIX);
			}
		return validFlag;
	}
	
	public static String lpad(String input, int length, char fillChar) {
		return org.apache.commons.lang3.StringUtils.leftPad(input, length,
				fillChar);
	}
	
	public static String nvl(String value, String replace) {
		return value != null ? value : replace;
	}
	
	public static String nvl2(String value, String exp1, String exp2 ) {
		return value != null ? exp1 : exp2;
	}
	
	public static String rpad(String input, int length, char fillChar) {
		return org.apache.commons.lang3.StringUtils.rightPad(input, length,
				fillChar);
	}
	
}
