package biz.shahed.freelance.heidi.mos.util;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yahoo.platform.yui.compressor.CssCompressor;

public class CssUtil {

	private static Logger log = LoggerFactory.getLogger(CssUtil.class);

	private static void minifyCSS(StringReader reader, StringWriter writer) throws IOException {
		CssCompressor cssCompressor = new CssCompressor(reader);
		cssCompressor.compress(writer, -1);
	}
	
	public static String minifyCSS(String css){
		StringReader reader = new StringReader(css);
		StringWriter writer = new StringWriter();
		try {
			minifyCSS(reader, writer);
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}
		return writer.toString();
	}
}
