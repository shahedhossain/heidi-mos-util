package biz.shahed.freelance.heidi.mos.util;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

public class CollectionUtil {

	public static List<?> merge(List<?>... all) {
		List<Object> merge = new ArrayList<Object>();
		for (List<?> list : all) {
			merge.addAll(list);
		}
		return merge;
	}

	public static List<?> add(List<Object> parent, List<?>... all) {
		for (List<?> list : all) {
			parent.addAll(list);
		}
		return parent;
	}
	
	public static TreeSet<String> toTreeSet(String[] values){
		TreeSet<String> set = new TreeSet<String>();
		for(String value:values){
			set.add(value);
		}
		return set;
	}
}
