package biz.shahed.freelance.heidi.mos.util.entity;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import biz.shahed.freelance.heidi.mos.util.ClassUtil;
import biz.shahed.freelance.heidi.mos.util.MethodUtil;

public class EntityFilter {

	private static final Logger log = LoggerFactory.getLogger(EntityFilter.class);

	private Object entity;

	public EntityFilter(Object entity) {		
		this.entity = entity;
	}

	public Object trimAndNullify() {
		for (Method getter : getGetters()) {
			if (MethodUtil.isString(getter)) {
				Method[] setters = getSetters();
				if (hasSetter(setters, getter)) {
					Method setter = getSetter(setters, getter);
					trimAndNullify(getter, setter);
				}
			}
		}
		return entity;
	}

	private Method[] getGetters() {
		Class<?> clazz = entity.getClass();
		List<Method> list = new ArrayList<Method>();
		for (Method method : clazz.getDeclaredMethods()) {
			if (isGetter(method)) {
				list.add(method);
			}
		}
		return toMethodArray(list);
	}

	private Method getSetter(Method[] setters, Method getter) {
		for (Method setter : setters) {
			String setterMethod = setter.getName().substring(3);
			String getterMethod = getter.getName().substring(3);
			if (setterMethod.equals(getterMethod)) {
				return setter;
			}
		}
		return null;
	}

	private Method[] getSetters() {
		Class<?> clazz = entity.getClass();
		List<Method> list = new ArrayList<Method>();
		for (Method method : clazz.getDeclaredMethods()) {
			if (isSetter(method)) {
				list.add(method);
			}
		}
		return toMethodArray(list);
	}

	private boolean hasSetter(Method[] setters, Method getter) {
		for (Method setter : setters) {
			String setterMethod = setter.getName().substring(3);
			String getterMethod = getter.getName().substring(3);
			if (setterMethod.equals(getterMethod)) {
				return true;
			}
		}
		return false;
	}

	private boolean isCollection(Method method) {
		return MethodUtil.isCollection(method);
	}

	private boolean isGetter(Method method) {
		return MethodUtil.isGetter(method) && !isCollection(method);
	}

	private boolean isSetter(Method method) {
		if(MethodUtil.isSetter(method)){
			Class<?> clazz = method.getParameterTypes()[0];
			if(!ClassUtil.isCollection(clazz)){
				return true;
			}
		}
		return false;
	}
	
	private void trimAndNullify(Method getter, Method setter) {
		try {
			String value = (String) getter.invoke(entity, new Object[] {});
			if (value != null) {
				value = value.trim();
				value = value.equals("") ? null : value;
				setter.invoke(entity, new Object[] { value });
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	private Method[] toMethodArray(List<Method> list) {
		int length = list.size();
		Method[] methods = new Method[length];
		for (int index = 0; index < length; index++) {
			methods[index] = list.get(index);
		}
		return methods;
	}

}
