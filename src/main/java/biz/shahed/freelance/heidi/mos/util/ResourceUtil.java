package biz.shahed.freelance.heidi.mos.util;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.security.CodeSource;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.zip.ZipException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ResourceUtil {
	
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(ResourceUtil.class);

	public static String[] getResourceListing(Class<?> clazz, String path) throws URISyntaxException, IOException {
		URL dirURL = clazz.getClassLoader().getResource(path);
		if (dirURL != null && dirURL.getProtocol().equals("file")) {

			return new File(dirURL.toURI()).list();
		}

		if (dirURL == null) {
			String me = clazz.getName().replace(".", "/") + ".class";
			dirURL = clazz.getClassLoader().getResource(me);
		}

		if (dirURL.getProtocol().equals("jar")) {

			String jarPath = dirURL.getPath().substring(5, dirURL.getPath().indexOf("!"));
			JarFile jar = new JarFile(URLDecoder.decode(jarPath, "UTF-8"));
			Enumeration<JarEntry> entries = jar.entries();
			Set<String> result = new HashSet<String>();
			while (entries.hasMoreElements()) {
				String name = entries.nextElement().getName();
				if (name.startsWith(path)) {
					String entry = name.substring(path.length());
					int checkSubdir = entry.indexOf("/");
					if (checkSubdir >= 0) {
						entry = entry.substring(0, checkSubdir);
					}
					result.add(entry);
				}
			}
			return result.toArray(new String[result.size()]);
		}

		throw new UnsupportedOperationException("Cannot list files for URL " + dirURL);
	}
	
	
	public static CodeSource getCodeSource(Class<?> clazz){
		return clazz.getProtectionDomain().getCodeSource();
	}
	
	public static URL getJarLocation(Class<?> clazz) {
		URL url = getCodeSource(clazz).getLocation();		
		return url;
	}
	
	public static File getJarFile(Class<?> clazz) throws Exception{
		URI uri   = getJarLocation(clazz).toURI();
		File file = new File(uri.getPath());
		if(!file.getAbsolutePath().endsWith("jar")){
			String message = "%s not a jar entry!";
			throw new Exception(String.format(message, clazz.getCanonicalName()));
		}
		return file;
	}
	
	public static boolean hasJarEntry(File jar, String fileName) throws ZipException, IOException {
		boolean isJarEntry = false;
		final JarFile jarFile = new JarFile(jar);
		isJarEntry = hasJarEntry(jarFile, fileName);
		jarFile.close();
		return isJarEntry;
	}

	private static boolean hasJarEntry(final JarFile jarFile, String entryName)
			throws ZipException, IOException {
		JarEntry entry = jarFile.getJarEntry(entryName);
		boolean flag = entry != null;
		return flag;
	}
	
	public static TreeSet<String> getEntries(Class<?> clazz) throws Exception {
		File jar = getJarFile(clazz);
		return getEntries(jar);
	}
	
	
	public static TreeSet<String> getEntries(File jar) throws IOException{
		final JarFile jarFile = new JarFile(jar);
		return getEntries(jarFile);
	}
	
	public static TreeSet<String> getEntries(final JarFile jarFile){
		TreeSet<String> resources = new TreeSet<String>();
		Enumeration<JarEntry> entries = jarFile.entries();
		while(entries.hasMoreElements()){
			String name = entries.nextElement().getName();
			resources.add(name);
		}
		return resources;
	}
	
	

}
