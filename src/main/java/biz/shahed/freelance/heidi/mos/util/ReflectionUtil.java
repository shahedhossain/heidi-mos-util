package biz.shahed.freelance.heidi.mos.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class ReflectionUtil {

	public static final int CALLER_CLASS = 2;
	public static final int CALLER_METHOD = 2;
	public static final int CURRENT_METHOD = 1;
	public static final int STACK_METHDOD = 0;

	public static Annotation[]  getAnnotation(Field field){
		return field.getAnnotations();
	}

	public static String getCallerMethod() {
		Throwable t = new Throwable();
		StackTraceElement[] stack = t.getStackTrace();
		return getCallerMethod(stack);
	}

	public static String getCallerMethod(StackTraceElement stackTraceElement) {
		return stackTraceElement.getMethodName();
	}
	
	public static String getCallerMethod(StackTraceElement[] stack) {
		StackTraceElement stackTraceElement = stack[CALLER_METHOD];
		return getCallerMethod(stackTraceElement);
	}
	
	public static Method[] getDeclaredMethods(Class<?> clazz) {
		return clazz.getDeclaredMethods();		
	}
	
	public static Field[] getFields(Class<?> clazz) {
		return clazz.getDeclaredFields();		
	}
	
}
