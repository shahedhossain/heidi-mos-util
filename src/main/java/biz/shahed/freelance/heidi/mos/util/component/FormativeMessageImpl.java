package biz.shahed.freelance.heidi.mos.util.component;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

@Component
public class FormativeMessageImpl implements FormativeMessage {

	private MessageSource messageSource;

	@Autowired
	public FormativeMessageImpl(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public String getMessage(String key, Object[] parameters, Locale locale) {
		return messageSource.getMessage(key, parameters, locale);
	}

	public String getMessage(String key, Object[] parameters) {
		Locale locale = LocaleContextHolder.getLocale();
		return getMessage(key, parameters, locale);
	}

	public String getMessage(String key, Object parameter) {
		Object[] parameters = new Object[] { parameter };
		return getMessage(key, parameters);
	}

	public String getMessage(String key) {
		Object[] parameters = new Object[] {};
		return getMessage(key, parameters);
	}

}
