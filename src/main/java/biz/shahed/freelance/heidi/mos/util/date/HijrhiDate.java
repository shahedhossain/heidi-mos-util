package biz.shahed.freelance.heidi.mos.util.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import biz.shahed.freelance.heidi.mos.util.DateUtil;
import biz.shahed.freelance.heidi.mos.util.StringUtil;

public class HijrhiDate {

	private static Logger logger = LoggerFactory.getLogger(HijrhiDate.class.getName());

	public HijrhiDate(Date gegorianDate) {

		int year = parseInt(gegorianDate, YYYY);
		int month = parseInt(gegorianDate, MM);
		int date = parseInt(gegorianDate, DD);
		
		arabicDate = hijriDate(year, month, date);
	}

	private Date hijriDate(int year, int month, int date) {
		
		double jd;
		double l;
		double n;
		double j;

		if ((year > 1582) || (year == 1582 && month > 10) || ((year == 1582) && (month == 10) && (date > 14))) {
			
			jd = intval((1461 * (year + 4800 + intval((month - 14) / 12))) / 4);
			jd += intval((367 * (month - 2 - 12 * (intval((month - 14) / 12)))) / 12);
			jd -= intval((3 * (intval((year + 4900 + intval((month - 14) / 12)) / 100))) / 4);
			jd += date - 32075;
			
		} else {
			
			jd = 367 * year;
			jd -= intval((7 * (year + 5001 + intval((month - 9) / 7))) / 4);
			jd += intval((275 * month) / 9);
			jd += date + 1729777;
			
		}

		l = jd - 1948440 + 10632;
		n = intval((l - 1) / 10631);
		l = l - 10631 * n + 354;

		j = (intval((10985 - l) / 5316)) * (intval((50 * l) / 17719));
		j += (intval(l / 5670)) * (intval((43 * l) / 15238));

		l -= (intval((30 - j) / 15)) * (intval((17719 * j) / 50));
		l -= (intval(j / 16)) * (intval((15238 * j) / 43));
		l += 29;

		month = intval((24 * l) / 709);
		date = intval(l - (intval((709 * month) / 24) - 1));
		year = intval(30 * n + j - 30);

		Date ardate = null;
		
		try {
			
			String source = "" + lpad(date) + "/" + lpad(month) + "/" + year;
			ardate = (new SimpleDateFormat(DD_MM_YYYY)).parse(source);
			
		} catch (ParseException e) {
			logger.error(e.getMessage(), e);
		}

		return ardate;
	}

	public int parseInt(Date date, String pattern) {
		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		return Integer.parseInt(formatter.format(date));
	}

	private String lpad(int input) {
		return StringUtil.lpad(input + "", 2, '0');
	}

	private int intval(double number) {
		return (int) number;
	}

	public Date getHijrhiDate() {
		return arabicDate;
	}

	private Date arabicDate;

	private final String YYYY = "yyyy";
	private final String MM = "MM";
	private final String DD = "dd";
	public static final String DD_MM_YYYY = DateUtil.DD_MM_YYYY;
}
