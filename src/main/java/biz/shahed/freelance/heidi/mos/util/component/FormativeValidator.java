package biz.shahed.freelance.heidi.mos.util.component;

import java.util.Collection;

import biz.shahed.freelance.heidi.mos.util.message.ViolatedMessage;

public interface FormativeValidator {

	public ViolatedMessage notEmpty(Collection<?> collection);
	
	public ViolatedMessage notEmpty(Collection<?> ... collections);

	public ViolatedMessage notEmpty(Collection<?> collection, String message);
	
	public ViolatedMessage notEmpty(String message, Collection<?> ... collections);

	public ViolatedMessage notNull(Object value);
	
	public ViolatedMessage notNull(Object ... objects);

	public ViolatedMessage notNull(Object value, String message);
	
	public ViolatedMessage notNull(String message, Object ... objects);

	public ViolatedMessage validate(Collection<?> collection);

	public ViolatedMessage validate(Object object);

	public ViolatedMessage validate(Object... objects);

}
