package biz.shahed.freelance.heidi.mos.util;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.FactoryBean;

public class ListMergerFactory implements FactoryBean<List<String>> {

	private List<String> list = new ArrayList<String>();

	public ListMergerFactory(List<List<String>> items) {
		for (List<String> item : items) {
			if (item != null) {
				list.addAll(item);
			}
		}
	}

	@Override
	public List<String> getObject() throws Exception {
		return list;
	}

	@Override
	public Class<?> getObjectType() {
		return list.getClass();
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

}
