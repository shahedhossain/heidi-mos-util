package biz.shahed.freelance.heidi.mos.util;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;


public class BeanUtil {
	
	private static final Logger log = LoggerFactory.getLogger(BeanUtil.class);
	
	
	public static Object bind(final Object target, final Map<String, Object> source, final Map<String, String> properties){
		final BeanWrapper trg = new BeanWrapperImpl(target);
		for(final String targetProperty : properties.keySet()){
			String sourceProperty = properties.get(targetProperty);
			Object value = source.get(sourceProperty);
			trg.setPropertyValue(targetProperty, value);
		}
		return target;
	}
	
	public static Object bind(final Object target, final Object source, final Map<String, String> properties){
		final BeanWrapper src = new BeanWrapperImpl(source);
		final BeanWrapper trg = new BeanWrapperImpl(target);
		
		for(final String targetProperty : properties.keySet()){
			String sourceProperty = properties.get(targetProperty);
			Object value = src.getPropertyValue(sourceProperty);
			trg.setPropertyValue(targetProperty, value);
		}
		
		return target;
	}
	
	public static Object cloneBean(final Object bean) {
		Object clone = null;
		try {
			clone = org.apache.commons.beanutils.BeanUtils.cloneBean(bean);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return clone;
	}

	public static Object copy(final Object target, final Map<String, Object> source) {
		final BeanWrapper trg = new BeanWrapperImpl(target);
		for (final String propertyName : source.keySet()) {
			Object value = source.get(propertyName);
			trg.setPropertyValue(propertyName, value);
		}
		return target;
	}
	
	public static Object copy(final Object target, final Map<String, Object> source, final String[] excludes) {
		final BeanWrapper trg = new BeanWrapperImpl(target);
		Set<String> properties = CollectionUtil.toTreeSet(excludes);
		for (final String propertyName : source.keySet()) {
			if(!properties.contains(propertyName)){
				Object value = source.get(propertyName);
				trg.setPropertyValue(propertyName, value);
			}
		}
		return target;
	}
	
	public static Object copy(final Object target, final Object source) {
		BeanUtils.copyProperties(target, source);
		return target;
	}

	public static Object copy(final Object target, final Object source, final String[] excludes) {
		BeanUtils.copyProperties(target, source, excludes);
		return target;
	}
	
	public static Object copyInclude(final Object target, final Map<String, Object> source, final String[] includes) {
		final BeanWrapper trg = new BeanWrapperImpl(target);
		Set<String> properties = CollectionUtil.toTreeSet(includes);
		for (final String propertyName : source.keySet()) {
			if(properties.contains(propertyName)){
				Object value = source.get(propertyName);
				trg.setPropertyValue(propertyName, value);
			}
		}
		return target;
	}

	public static Object copyInclude(final Object target, final Object source, final String[] includes) {
		final BeanWrapper src = new BeanWrapperImpl(source);
		final BeanWrapper trg = new BeanWrapperImpl(target);
		for (final String propertyName : includes) {
			Object value = src.getPropertyValue(propertyName);
			trg.setPropertyValue(propertyName, value);
		}
		return target;
	}
	
	public static Object copyMatch(final Object target, final Map<String, Object> source) {
		final BeanWrapper trg = new BeanWrapperImpl(target);
		for (final String property : source.keySet()) {
			Object value = source.get(property);
			if(ClassUtil.hasProperty(target.getClass(), property)){
				trg.setPropertyValue(property, value);
			}			
		}
		return target;
	}
	
	public static Object copyMatch(final Object target, final Map<String, Object> source, final String[] excludes) {
		Set<String> excludeProperties = CollectionUtil.toTreeSet(excludes);
		final BeanWrapper trg = new BeanWrapperImpl(target);
		for (final String property : source.keySet()) {
			if(!excludeProperties.contains(property)){
				Object value = source.get(property);
				if(ClassUtil.hasProperty(target.getClass(), property)){
					trg.setPropertyValue(property, value);
				}
			}
		}
		return target;
	}
		
	public static Object copyMatch(final Object target, final Object source) {
		List<String> properties = ClassUtil.getProperties(source.getClass());
		final BeanWrapper src = new BeanWrapperImpl(source);
		final BeanWrapper trg = new BeanWrapperImpl(target);
		
		for (final String property : properties) {
			Object value = src.getPropertyValue(property);
			if(ClassUtil.hasProperty(target.getClass(), property)){
				trg.setPropertyValue(property, value);
			}			
		}
		return target;
	}
	
	public static Object copyMatch(final Object target, final Object source, final String[] excludes) {
		List<String> properties = ClassUtil.getProperties(source.getClass());
		Set<String> excludeProperties = CollectionUtil.toTreeSet(excludes);
		final BeanWrapper src = new BeanWrapperImpl(source);
		final BeanWrapper trg = new BeanWrapperImpl(target);
		
		for (final String property : properties) {
			if(!excludeProperties.contains(property)){
				Object value = src.getPropertyValue(property);
				if(ClassUtil.hasProperty(target.getClass(), property)){
					trg.setPropertyValue(property, value);
				}
			}
		}
		return target;
	}
		
	public static Field getIdField(final Object object) {
		Class<?> clazz = object.getClass();
		return ClassUtil.getIdField(clazz);
	}
	
	public static String getIdProperty(final Object object) {
		Class<?> clazz = object.getClass();
		return ClassUtil.getIdProperty(clazz);
	}
	
	public static Object getIdPropertyValue(final Object object){
		final BeanWrapper wrapper = new BeanWrapperImpl(object);
		String property = getIdProperty(object);
		return wrapper.getPropertyValue(property);
	}
	
	public static List<String> getProperties(final Class<?> clazz) {
		return ClassUtil.getProperties(clazz);
	}
	
	public static List<String> getProperties(final Object object) {
		Class<?> clazz = object.getClass();
		return getProperties(clazz);
	}
	
	public static boolean isProperty(final Object object, Field field) {
		Class<?> clazz = object.getClass();
		return ClassUtil.isProperty(clazz, field);		
	}
	
}
