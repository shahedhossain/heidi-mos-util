package biz.shahed.freelance.heidi.mos.util;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.security.CodeSource;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClassUtil {

	private static final Logger log = LoggerFactory.getLogger(ClassUtil.class);
	public static final String META_GROOVY = "META-INF/m[0-9]{2}001/m[0-9]{2}[b|i|q|r|s|t]{1}001/groovy/[\\w]*+";
	public static final String META_INF    = "META-INF/";
	public static final String META_MAVEN  = "META-INF/maven/";
	public static final String META_SPRING = "META-INF/maven/m[0-9]{2}001/spring/[\\w\\-]*+";	
	public static final String UTF_8       = "UTF-8";

	public static boolean equalsType(Class<?> clazz, String canonicalName) {
		return clazz.getCanonicalName().equals(canonicalName);
	}

	private static CodeSource getCodeSource(Class<?> clazz) {
		return clazz.getProtectionDomain().getCodeSource();
	}

	private static File getFile(Class<?> clazz) throws Exception {
		URI uri = getURI(clazz);
		File file = new File(URLDecoder.decode(uri.getPath(), UTF_8));
		return file;
	}

	public static Field getIdField(Class<?> clazz) {
		for (Field field : clazz.getDeclaredFields()) {
			if (FieldUtil.isIdField(field)) {
				if (FieldUtil.isPrivate(field)) {
					if (isProperty(clazz, field)) {
						return field;
					}
				}
			}
		}
		return null;
	}

	public static String getIdProperty(Class<?> clazz) {
		Field field = getIdField(clazz);
		return field != null ? field.getName() : null;
	}
	
	public static File getJarFile(Class<?> clazz) throws Exception {
		File file = getFile(clazz);
		if(!isJarFile(file)){
			String message = "%s not a jar entry!";
			throw new Exception(String.format(message, clazz.getCanonicalName()));
		}
		return file;
	}

	public static TreeSet<String> getMavenResources(Class<?> clazz) throws Exception {
		TreeSet<String> mavenresources = new TreeSet<String>();
		for(String resource:getMetaResources(clazz)){
			if(resource.startsWith(META_MAVEN)){
				mavenresources.add(resource);
			}
		}
		return mavenresources;
	}
	
	public static TreeSet<String> getMetaResources(Class<?> clazz) throws Exception {
		TreeSet<String> metaresources = new TreeSet<String>();
		for(String resource:getResources(clazz)){
			if(resource.startsWith(META_INF)){
				metaresources.add(resource);
			}
		}
		return metaresources;
	}
	
	public static List<String> getProperties(Class<?> clazz) {
		List<String> properties = new ArrayList<String>();
		for (Field field : clazz.getDeclaredFields()) {
			if (FieldUtil.isPrivate(field)) {
				if(isProperty(clazz, field)){
					properties.add(field.getName());
				}
			}
		}
		return properties;
	}
	
	public static TreeSet<String> getPureResources(Class<?> clazz) throws Exception {
		TreeSet<String> resources = new TreeSet<String>();
		for(String resource:getMetaResources(clazz)){
			if(!resource.startsWith(META_MAVEN)){
				resources.add(resource);
			}
		}
		return resources;
	}
	
	public static TreeSet<String> getResources(Class<?> clazz) throws Exception {
		File jar = getJarFile(clazz);
		return JarUtil.getResources(jar);
	}
	
	public static TreeSet<String> getSpringContext(Class<?> clazz) throws Exception {
		TreeSet<String> resources = new TreeSet<String>();
		for(String resource:getMetaResources(clazz)){
			if(PatternUtil.isMatch(resource, META_SPRING)){
				resources.add(resource);
			}
		}
		return resources;
	}
	
	private static URI getURI(Class<?> clazz) throws URISyntaxException,	Exception {
		return getURL(clazz).toURI();
	} 
		
	private static URL getURL(Class<?> clazz) throws Exception {
		URL url = getCodeSource(clazz).getLocation();
		if (url != null && !url.getProtocol().equals("file")) {
			String message = "%s not a jar entry!";
			throw new Exception(String.format(message, clazz.getCanonicalName()));
		}
		return getCodeSource(clazz).getLocation();
	}

	public static boolean hasProperty(Class<?> clazz, String propertyName){
		List<String> properties = getProperties(clazz);
		for(String property:properties){
			if(property.equals(propertyName)){
				return true;
			}
		}
		return false;
	}

	public static boolean isBoolean(Class<?> clazz) {
		return isBooleanPrimitive(clazz) || isBooleanWrapper(clazz);
	}

	public static boolean isBoolean(Class<?> clazz, boolean isPrimitive) {
		return isPrimitive ? isBooleanPrimitive(clazz) : isBoolean(clazz);
	}

	public static boolean isBooleanPrimitive(Class<?> clazz) {
		String canonicalName = "boolean";
		return isPrimitive(clazz) ? equalsType(clazz, canonicalName) : false;
	}

	public static boolean isBooleanWrapper(Class<?> clazz) {
		String canonicalName = "java.lang.Boolean";
		return equalsType(clazz, canonicalName);
	}

	public static boolean isByte(Class<?> clazz) {
		return isBytePrimitive(clazz) || isByteWrapper(clazz);
	}

	public static boolean isByte(Class<?> clazz, boolean isPrimitive) {
		return isPrimitive ? isBytePrimitive(clazz) : isByte(clazz);
	}
	
	public static boolean isBytePrimitive(Class<?> clazz) {
		String canonicalName = "byte";
		return isPrimitive(clazz) ? equalsType(clazz, canonicalName) : false;
	}

	public static boolean isByteWrapper(Class<?> clazz) {
		String canonicalName = "java.lang.Byte";
		return equalsType(clazz, canonicalName);
	}

	public static boolean isCharacter(Class<?> clazz) {
		return isCharacterPrimitive(clazz) || isCharacterWrapper(clazz);
	}

	public static boolean isCharacter(Class<?> clazz, boolean isPrimitive) {
		return isPrimitive ? isCharacterPrimitive(clazz) : isCharacter(clazz);
	}

	public static boolean isCharacterPrimitive(Class<?> clazz) {
		String canonicalName = "char";
		return isPrimitive(clazz) ? equalsType(clazz, canonicalName) : false;
	}

	public static boolean isCharacterWrapper(Class<?> clazz) {
		String canonicalName = "java.lang.Character";
		return equalsType(clazz, canonicalName);
	}

	public static boolean isCollection(Class<?> clazz) {
		String canonicalName = "java.util.Collection";
		if(equalsType(clazz, canonicalName)){
			return true;
		}
		return (isList(clazz) || isSet(clazz) || isMap(clazz));
	}

	public static boolean isDouble(Class<?> clazz) {
		return isDoublePrimitive(clazz) || isDoubleWrapper(clazz);
	}

	public static boolean isDouble(Class<?> clazz, boolean isPrimitive) {
		return isPrimitive ? isDoublePrimitive(clazz) : isDouble(clazz);
	}

	public static boolean isDoublePrimitive(Class<?> clazz) {
		String canonicalName = "double";
		return isPrimitive(clazz) ? equalsType(clazz, canonicalName) : false;
	}

	public static boolean isDoubleWrapper(Class<?> clazz) {
		String canonicalName = "java.lang.Double";
		return equalsType(clazz, canonicalName);
	}

	public static boolean isFloat(Class<?> clazz) {
		return isFloatPrimitive(clazz) || isFloatWrapper(clazz);
	}

	public static boolean isFloat(Class<?> clazz, boolean isPrimitive) {
		return isPrimitive ? isFloatPrimitive(clazz) : isFloat(clazz);
	}

	public static boolean isFloatPrimitive(Class<?> clazz) {
		String canonicalName = "float";
		return isPrimitive(clazz) ? equalsType(clazz, canonicalName) : false;
	}

	public static boolean isFloatWrapper(Class<?> clazz) {
		String canonicalName = "java.lang.Float";
		return equalsType(clazz, canonicalName);
	}

	public static boolean isInteger(Class<?> clazz) {
		return isIntegerPrimitive(clazz) || isIntegerWrapper(clazz);
	}

	public static boolean isInteger(Class<?> clazz, boolean isPrimitive) {
		return isPrimitive ? isIntegerPrimitive(clazz) : isInteger(clazz);
	}

	public static boolean isIntegerPrimitive(Class<?> clazz) {
		String canonicalName = "int";
		return isPrimitive(clazz) ? equalsType(clazz, canonicalName) : false;
	}

	public static boolean isIntegerWrapper(Class<?> clazz) {
		String canonicalName = "java.lang.Integer";
		return equalsType(clazz, canonicalName);
	}

	public static boolean isJarEntry(Class<?> clazz) {
		boolean isJarEntry = false;
		try {
			File file = getFile(clazz);
			isJarEntry =isJarFile(file);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return isJarEntry;
	}

	private static boolean isJarFile(File file){
		boolean isJarFile = false;
		if(file != null && !file.isDirectory()){
			isJarFile = file.getAbsolutePath().endsWith(".jar");
		}
		return isJarFile;
	}

	public static boolean isList(Class<?> clazz) {
		String canonicalName = "java.util.List";
		return equalsType(clazz, canonicalName);
	}

	public static boolean isLong(Class<?> clazz) {
		return isLongPrimitive(clazz) || isLongWrapper(clazz);
	}

	public static boolean isLong(Class<?> clazz, boolean isPrimitive) {
		return isPrimitive ? isLongPrimitive(clazz) : isLong(clazz);
	}
	
	public static boolean isLongPrimitive(Class<?> clazz) {
		String canonicalName = "long";
		return isPrimitive(clazz) ? equalsType(clazz, canonicalName) : false;
	}

	public static boolean isLongWrapper(Class<?> clazz) {
		String canonicalName = "java.lang.Long";
		return equalsType(clazz, canonicalName);
	}

	public static boolean isMap(Class<?> clazz) {
		String canonicalName = "java.util.Map";
		return equalsType(clazz, canonicalName);
	}
	
	public static boolean isPrimitive(Class<?> clazz) {
		return clazz.isPrimitive();
	}
	
	public static boolean isProperty(Class<?> clazz, Field field){
		if (FieldUtil.isPrivate(field) && !FieldUtil.isStatic(field) && !FieldUtil.isFinal(field)) {
			String getterName = FieldUtil.getGetterName(field);
			String setterName = FieldUtil.getSetterName(field);
			try {
				Method getterMethod = clazz.getDeclaredMethod(getterName);
				Method setterMethod = clazz.getDeclaredMethod(setterName, field.getType());
				if (getterMethod != null && setterMethod != null) {
					if (MethodUtil.isGetter(getterMethod) && MethodUtil.isSetter(setterMethod)) {
						return true;
					}
				}
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			}
		}
		return false;		
	}
	
	public static boolean isSet(Class<?> clazz) {
		String canonicalName = "java.util.Set";
		return equalsType(clazz, canonicalName);
	}

	public static boolean isShort(Class<?> clazz) {
		return isShortPrimitive(clazz) || isShortWrapper(clazz);
	}

	public static boolean isShort(Class<?> clazz, boolean isPrimitive) {
		return isPrimitive ? isShortPrimitive(clazz) : isShort(clazz);
	}
	
	public static boolean isShortPrimitive(Class<?> clazz) {
		String canonicalName = "short";
		return isPrimitive(clazz) ? equalsType(clazz, canonicalName) : false;
	}
		
	public static boolean isShortWrapper(Class<?> clazz) {
		String canonicalName = "java.lang.Short";
		return equalsType(clazz, canonicalName);
	}
	
	public static boolean isString(Class<?> clazz) {
		String canonicalName = "java.lang.String";
		return equalsType(clazz, canonicalName);
	}
	
	public static boolean isVoid(Class<?> clazz){
		return clazz.equals(Void.TYPE);
	}
	
	public static boolean isWrapper(Class<?> clazz) {
		boolean isWrapper = !isPrimitive(clazz);
		if(isWrapper){
			isWrapper = isBooleanWrapper(clazz)
					 || isByteWrapper(clazz)
					 || isShortWrapper(clazz)
					 || isCharacterWrapper(clazz)
					 || isIntegerWrapper(clazz)
					 || isLongWrapper(clazz)
					 || isFloatWrapper(clazz)
					 || isDoubleWrapper(clazz);
					 
		}
		return isWrapper;
	}

}
