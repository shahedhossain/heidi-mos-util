package biz.shahed.freelance.heidi.mos.util;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;

import org.hibernate.Criteria;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import biz.shahed.freelance.heidi.mos.util.entity.EntityFilter;

public class EntityUtil {

	private static final Logger log = LoggerFactory.getLogger(EntityUtil.class);
	
	public static Criteria addExample(Criteria criteria, Example where) {
		return criteria.add(where);
	}

	public static Criteria addExample(Criteria criteria, Example where, String propertyName, Object value) {
		if (value != null) {
			criteria.add(Restrictions.ge(propertyName, value));
		} 
		criteria.addOrder(Order.asc(propertyName));
		return criteria.add(where);
	}

	public static Criteria addExample(Criteria criteria, Object entity) {
		String property = BeanUtil.getIdProperty(entity);
		Object provalue = BeanUtil.getIdPropertyValue(entity);
		if (provalue != null) {
			criteria.add(Restrictions.ge(property, provalue));
		}
		criteria.addOrder(Order.asc(property));
		return criteria.add(getExample(entity));
	}

	public static Criteria addExample(Criteria criteria, Object entity, String propertyName, Object value) {
		if (value != null) {
			criteria.add(Restrictions.ge(propertyName, value));
		}
		criteria.addOrder(Order.asc(propertyName));
		return addExample(criteria, entity);
	}

	public static Object filterEntity(Object entity) {
		EntityFilter filter = new EntityFilter(entity);
		return filter.trimAndNullify();
	}

	public static String getColumnName(Class<?> clazz, String fieldName) {
		String columnName = fieldName;
		try {
			Field field = clazz.getDeclaredField(fieldName);
			Column column = field.getAnnotation(Column.class);
			if (column != null) {
				columnName = column.name();
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return columnName;
	}

	public static Criteria addRestrictions(final Criteria criteria, Map<String, Object> properties) {
		return addRestrictions(criteria, properties, false);
	}

	public static Criteria addRestrictions(final Criteria criteria, Map<String, Object> properties, boolean strictDate) {
		for (String property : properties.keySet()) {
			Object provalue = properties.get(property);
			if (provalue != null) {
				if (provalue instanceof String) {
					String value = (String) provalue;
					if (!value.isEmpty()) {
						criteria.add(Restrictions.ilike(property, value, MatchMode.START));
					}
				} else if (provalue instanceof Date) {
					Date date = (Date) provalue;
					if (strictDate) {
						criteria.add(Restrictions.eq(property, date));
					} else {
						Date lo = DateUtil.maxDate(date);
						Date hi = DateUtil.minDate(date);
						criteria.add(Restrictions.between(property, lo, hi));
					}
				} else {
					criteria.add(Restrictions.eq(property, provalue));
				}
			}
		}
		return criteria;
	}

	public static Example getExample(Object entity) {
		Object filter = filterEntity(entity);
		Example where = Example.create(filter);		
		return where.ignoreCase().enableLike(MatchMode.START);
	}
	
	public static Object getSingleResult(Criteria criteria) {
		Object object = null;
		List<?> list = criteria.list();
		if (list != null && !list.isEmpty()) {
			object = list.get(0);
		}
		return object;
	}

	public static boolean hasChild(Collection<?> collection) {
		return collection != null && !collection.isEmpty();
	}

	public static boolean hasChild(Collection<?>... collections) {
		for (Collection<?> collection : collections) {
			if (hasChild(collection)) {
				return true;
			}
		}
		return false;
	}

	public static boolean hasChild(Map<String, ?> map) {
		return map != null && !map.isEmpty();
	}

	public static boolean hasChild(Map<String, ?>... maps) {
		for (Map<String, ?> map : maps) {
			if (hasChild(map)) {
				return true;
			}
		}
		return false;
	}

	public static boolean isColumnString(Class<?> clazz, String fieldName) {
		boolean stringFlag = false;
		try {
			Field field = clazz.getDeclaredField(fieldName);
			return FieldUtil.isString(field);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return stringFlag;
	}
	
	public static boolean isEquals(Object self, Object other) {
		if (self == null && other != null) {
			return false;
		} else if (self != null && other == null) {
			return false;
		} else if (self != null && other != null) {
			return self.equals(other);
		}
		return true;
	}

	public static String toString(Class<?> clazz, String propertyName, Object value) {
		return String.format("%s[%s=%s]", clazz.getName(), propertyName, value);
	}


}