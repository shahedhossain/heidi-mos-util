package biz.shahed.freelance.heidi.mos.util.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HttpsCookieFilter implements Filter {

	private static Logger log = LoggerFactory.getLogger(HttpsCookieFilter.class);
	
	public static final String CHARACTER_ENDCODE = "UTF-8";
	public static final String WEB    = "www.formativesoft.com";
	public static final String EMAIL  = "formative.soft@gmail.com";
	public static final String VENDOR = "Formative Soft Ltd";
	
	@Override
	public void init(FilterConfig cfg) throws ServletException {
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,FilterChain chain) throws IOException, ServletException {

		final HttpServletRequest req = (HttpServletRequest) request;
		final HttpServletResponse res = (HttpServletResponse) response;

		req.setCharacterEncoding(CHARACTER_ENDCODE);
		res.setCharacterEncoding(CHARACTER_ENDCODE);
		res.setHeader("Web", WEB);
		res.setHeader("Email", EMAIL);
		res.setHeader("Vendor", VENDOR);

		final HttpSession session = req.getSession(false);

		if (session != null) {
			setCookie(req, res);
		}
		try {
			chain.doFilter(req, res);
		} catch (IllegalStateException e) {
			log.warn("HttpsCookieFilter redirect problem!", e);
		}
	}
	
	private void setCookie(HttpServletRequest request, HttpServletResponse response) {
		String sessionId = request.getSession(false).getId();
		Cookie cookie = new Cookie("JSESSIONID", sessionId);
		cookie.setMaxAge(-1);		
		cookie.setSecure(false);
		response.addCookie(cookie);
		cookie.setPath(getCookiePath(request));
	}

	private String getCookiePath(HttpServletRequest request) {
		String contextPath = request.getContextPath();
		return contextPath.length() > 0 ? contextPath : "/";
	}
	
	@Override
	public void destroy() {
		
	}		

}
