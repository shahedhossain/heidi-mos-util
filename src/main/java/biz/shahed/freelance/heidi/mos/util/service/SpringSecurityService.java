package biz.shahed.freelance.heidi.mos.util.service;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.userdetails.UserDetails;

public interface SpringSecurityService {

	String passwordEncoder(UserDetails user);
	
	Authentication getAuthentication();

	Object getPrincipal();

	SecurityContext getSecurityContext();

	boolean isAjax(HttpServletRequest request);
	
	String getCurrentUser();

	boolean isLoggedIn();

}
