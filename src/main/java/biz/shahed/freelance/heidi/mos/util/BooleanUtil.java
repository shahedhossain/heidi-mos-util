package biz.shahed.freelance.heidi.mos.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BooleanUtil {

	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(BooleanUtil.class);
	
	public static boolean allFalse(boolean[] bool) {
		boolean returnFlag = false;
		for (boolean flag : bool) {
			returnFlag = returnFlag || flag;
		}
		return !returnFlag;
	}

	public static boolean allTrue(boolean[] bool) {
		boolean returnFlag = true;
		for (boolean flag : bool) {
			returnFlag = returnFlag && flag;
		}
		return returnFlag;
	}

	public static boolean anyOneFalse(boolean[] bool) {
		boolean returnFlag = true;
		for (boolean flag : bool) {
			returnFlag = returnFlag && flag;
		}
		return !returnFlag;
	}

	public static boolean anyOneTrue(boolean[] bool) {
		boolean returnFlag = false;
		for (boolean flag : bool) {
			returnFlag = returnFlag || flag;
		}
		return returnFlag;
	}

	public static int firstFalseIndex(boolean[] bool) {
		int returnIndex = -1;
		for (int i = 0; i < bool.length; i++) {
			if (!bool[i]) {
				returnIndex = i;
				return returnIndex;
			}
		}
		return returnIndex;
	}

	public static int firstTrueIndex(boolean[] bool) {
		int returnIndex = -1;
		for (int i = 0; i < bool.length; i++) {
			if (bool[i]) {
				returnIndex = i;
				return returnIndex;
			}
		}
		return returnIndex;
	}

	public static boolean isBool(String bool) {
		boolean boolFlag = false;
		if(bool != null && !bool.isEmpty()) {
			boolFlag = bool.trim().toLowerCase().matches("[true|false]");
		}
		return boolFlag;
	}
	
	public static int lastFalseIndex(boolean[] bool) {
		int returnIndex = -1;
		for (int i = 0; i < bool.length; i++) {
			if (!bool[i]) {
				returnIndex = i;
			}
		}
		return returnIndex;
	}

	public static int lastTrueIndex(boolean[] bool) {
		int returnIndex = -1;
		for (int i = 0; i < bool.length; i++) {
			if (bool[i]) {
				returnIndex = i;
			}
		}
		return returnIndex;
	}
	
	public static boolean parse(String input) {
		return Boolean.getBoolean(input);
	}
	
	public static boolean parseBool(String bool) {
		boolean boolFlag = false;		
		if(isBool(bool)) {
			bool = bool.toLowerCase();
			boolFlag = bool.equals("true");
		}		
		return boolFlag;	
	}
}
