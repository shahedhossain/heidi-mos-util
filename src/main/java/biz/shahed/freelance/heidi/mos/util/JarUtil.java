package biz.shahed.freelance.heidi.mos.util;

import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.TreeSet;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.zip.ZipException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JarUtil {

	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(JarUtil.class);	
	
	private static JarFile getJarFile(File jar) throws IOException{
		return new JarFile(jar);
	}
	
	public static boolean hasResource(File jar, String resource) throws ZipException, IOException {
		final JarFile jarFile = getJarFile(jar);
		boolean isJarEntry    = hasResource(jarFile, resource);
		jarFile.close();
		return isJarEntry;
	}

	private static boolean hasResource(final JarFile jarFile, String resource)	throws ZipException, IOException {
		JarEntry entry = jarFile.getJarEntry(resource);
		return entry != null;
	}
	
	public static TreeSet<String> getResources(File jar) throws IOException{
		TreeSet<String> resources = new TreeSet<String>();
		final JarFile jarFile = getJarFile(jar);
		resources = getResources(jarFile);
		jarFile.close();
		return resources;
	}
	
	private static TreeSet<String> getResources(final JarFile jarFile){
		TreeSet<String> resources     = new TreeSet<String>();
		Enumeration<JarEntry> entries = jarFile.entries();
		while(entries.hasMoreElements()){
			String name = entries.nextElement().getName();
			resources.add(name);
		}
		return resources;
	}
}
