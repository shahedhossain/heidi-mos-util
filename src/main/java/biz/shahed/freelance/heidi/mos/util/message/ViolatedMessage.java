package biz.shahed.freelance.heidi.mos.util.message;

import java.util.List;

public interface ViolatedMessage {

	public String getMessage();

	public List<String> getMessages();

	public boolean isValid();

	public boolean isViolated();
	
	public boolean notViolated();

}
