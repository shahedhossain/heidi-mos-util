package biz.shahed.freelance.heidi.mos.util.env;

public interface HeidiMos {

	public String DEVELOPMENT 	= "dev";
	public String PRODUCTION  	= "prod";
	public String DEBUG		 	= "debug";
	
	public String getEnvironment();
	
	public boolean isDevelopment();
	
	public boolean isProduction();
	
	public boolean isDebug();
	
}
