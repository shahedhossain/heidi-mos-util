package biz.shahed.freelance.heidi.mos.util;

import java.lang.reflect.Field;
import java.sql.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TypeUtil {
	
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(TypeUtil.class);

	public static boolean equalsType(Class<?> type, Class<?> clazz){
		return type.equals(clazz);
	}

	public static boolean equalsType(Field field, Class<?> clazz){
		Class<?> type = field.getType();
		return type.equals(clazz);
	}
	
	public static boolean equalsTypeOfAny(Class<?> type, Class<?>...classes){
		boolean isTypeOf = false;
		if(!type.isPrimitive()){
			for(Class<?> clazz:classes){
				isTypeOf = isTypeOf || equalsType(type, clazz);
			}
		}
		return isTypeOf;
	}
	
	public static boolean equalsTypeOfAny(Field field, Class<?>...classes){
		Class<?> type = field.getType();		
		return equalsTypeOfAny(type, classes);
	}
	
	public static boolean isBoolean(Field field){
		return FieldUtil.isBoolean(field);
	}
	
	public static boolean isFloat(Field field) {
		boolean isFloat = false;
		isFloat = isFloat || isPrimitiveFloat(field);
		isFloat = isFloat || isWrapperFloat(field);
		return isFloat;
	}
	
	public static boolean isInt(Field field){
		boolean isInt = false;
		isInt = isInt || isPrimitiveInt(field);
		isInt = isInt || isWrapperInt(field);
		return isInt;
	}
	
	public static boolean isJavaType(Field field){
		boolean isPrimitiveOrWrapperType = false;
		isPrimitiveOrWrapperType = isPrimitiveOrWrapperType || isWrapper(field);
		isPrimitiveOrWrapperType = isPrimitiveOrWrapperType || isPrimitive(field);
		return isPrimitiveOrWrapperType;
	}
	
	public static boolean isPrimitive(Field field){
		return FieldUtil.isPrimitive(field);
	}
	
	public static boolean isPrimitiveBoolean(Field field){
		return FieldUtil.isBooleanPrimitive(field);
	}
	
	public static boolean isPrimitiveChar(Field field){
		return FieldUtil.isCharacterPrimitive(field);
	}	
	
	public static boolean isPrimitiveFloat(Field field){
		boolean isPrimitiveFloat = false;
		if(isPrimitive(field)){
			isPrimitiveFloat = isPrimitiveFloat || FieldUtil.isFloatPrimitive(field);
			isPrimitiveFloat = isPrimitiveFloat || FieldUtil.isDoublePrimitive(field);	
		}
		return isPrimitiveFloat;
	}
		
	public static boolean isPrimitiveInt(Field field){
		boolean isPrimitiveInt = false;
		if(isPrimitive(field)){			
			isPrimitiveInt = isPrimitiveInt || FieldUtil.isIntegerPrimitive(field);
			isPrimitiveInt = isPrimitiveInt || FieldUtil.isShortPrimitive(field);
			isPrimitiveInt = isPrimitiveInt || FieldUtil.isBytePrimitive(field);
			isPrimitiveInt = isPrimitiveInt || FieldUtil.isLongPrimitive(field);
		}
		return isPrimitiveInt;
	}
	
	public static boolean isString(Field field){
		boolean isString = false;
		isString = isString || isWrapperString(field);
		isString = isString || isPrimitiveChar(field);
		isString = isString || isWrapperChar(field);
		return isString;
	}
		
	public static boolean isWrapper(Field field){
		return FieldUtil.isWrapper(field);	
	}
	
	public static boolean isWrapperBoolean(Field field){
		return FieldUtil.isBooleanWrapper(field);
	}
	
	public static boolean isWrapperChar(Field field){
		return FieldUtil.isCharacterWrapper(field);
	}
	
	public static boolean isWrapperFloat(Field field){
		boolean isWrapperFloat = false;
		if(isWrapper(field)){
			isWrapperFloat = isWrapperFloat || FieldUtil.isFloatWrapper(field);
			isWrapperFloat = isWrapperFloat || FieldUtil.isDoubleWrapper(field);	
		}
		return isWrapperFloat;
	}
	
	public static boolean isWrapperInt(Field field){
		boolean isWrapperInt = false;
		if(isWrapper(field)){			
			isWrapperInt = isWrapperInt || FieldUtil.isIntegerWrapper(field);
			isWrapperInt = isWrapperInt || FieldUtil.isShortWrapper(field);
			isWrapperInt = isWrapperInt || FieldUtil.isByteWrapper(field);
			isWrapperInt = isWrapperInt || FieldUtil.isLongWrapper(field);
		}
		return isWrapperInt;
	}
	
	public static boolean isWrapperString(Field field){
		return FieldUtil.isString(field);
	}
	
	public static boolean isWrapperType(Field field){
		return FieldUtil.isWrapper(field);
	}
	
	public static boolean isDate(Field field){
		if(!isJavaType(field)){
			return equalsType(field, Date.class);
		}
		return false;
	}
	
}
