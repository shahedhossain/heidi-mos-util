package biz.shahed.freelance.heidi.mos.util.upload;

import org.springframework.web.multipart.MultipartFile;

public class SingleFileUpload {

	private MultipartFile file;

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

}
