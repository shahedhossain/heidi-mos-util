package biz.shahed.freelance.heidi.mos.util.component;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import biz.shahed.freelance.heidi.mos.util.BeanUtil;

@Component
public class BindBeanDataImpl<K> implements BindBeanData<K> {

	private static final Logger log = LoggerFactory.getLogger(BindBeanDataImpl.class);
	
	public K bind(final Class<K> clazz, final Map<String, Object> source, final Map<String, String> properties){
		K target = getInstance(clazz);
		return bind(target, source, properties);
	}
	
	@Override
	public K bind(final Class<K> clazz, final Map<String, Object> source, final String[] pairProperties){
		K target = getInstance(clazz);	
		return bind(target, source, pairProperties);
	}
		
	@Override
	public K bind(final Class<K> clazz, final Object source, final Map<String, String> properties){
		K target = getInstance(clazz);	
		return bind(target, source, properties);
	}
	
	@Override
	public K bind(final Class<K> clazz, final Object source, final String[] pairProperties){
		K target = getInstance(clazz);		
		return bind(target, source, pairProperties);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public K bind(final K target, final Map<String, Object> source, final Map<String, String> properties){
		return (K)BeanUtil.bind(target, source, properties);
	}
	
	@Override
	public K bind(final K target, final Map<String, Object> source, final String[] pairProperties){
		Map<String, String> properties = new HashMap<String, String>();
		for(String pairProperty:pairProperties){
			putProperty(properties, pairProperty);
		}		
		return bind(target, source, properties);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public K bind(final K target, final Object source, final Map<String, String> properties){
		return (K)BeanUtil.bind(target, source, properties);
	}
	
	@Override
	public K bind(final K target, final Object source, final String[] pairProperties){
		Map<String, String> properties = new HashMap<String, String>();
		for(String pairProperty:pairProperties){
			putProperty(properties, pairProperty);
		}		
		return bind(target, source, properties);
	}
			
	@Override
	public K bind(final Object source, final Map<String, String> properties){
		return bind(source, properties);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public K cloneBean(final K bean) {
		return (K)BeanUtil.cloneBean(bean);
	}
	
	@Override
	public K copy(Class<K> clazz, Map<String, Object> source) {
		K target = getInstance(clazz);
		return copy(target, source);
	}
	
	@Override
	public K copy(Class<K> clazz, Map<String, Object> source, String[] excludes) {
		K target = getInstance(clazz);
		return copy(target, source, excludes);
	}

	@Override
	public K copy(Class<K> clazz, Map<String, Object> source, String[] includes, String[] excludes) {
		K target = getInstance(clazz);
		return copy(target, source, includes, excludes);
	}
	
	@Override
	public K copy(Class<K> clazz, Object source) {
		K target = getInstance(clazz);
		return copy(target, source);
	}

	@Override
	public K copy(Class<K> clazz, Object source, String[] excludes) {
		K target = getInstance(clazz);
		return copy(target, source, excludes);
	}
	
	@Override
	public K copy(Class<K> clazz, Object source, String[] includes, String[] excludes) {
		K target = getInstance(clazz);
		return copy(target, source, includes, excludes);
	}

	@Override
	@SuppressWarnings("unchecked")
	public K copy(K target, Map<String, Object> source) {
		return (K) BeanUtil.copyMatch(target, source);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public K copy(K target, Map<String, Object> source, String[] excludes) {
		return (K) BeanUtil.copyMatch(target, source, excludes);
	}

	@Override
	public K copy(K target, Map<String, Object> source, String[] includes, String[] excludes) {
		bind(target, source, includes);
		return copy(target, source, excludes);
	}

	@Override
	@SuppressWarnings("unchecked")
	public K copy(K target, Object source) {
		return (K) BeanUtil.copyMatch(target, source);
	}

	@Override
	@SuppressWarnings("unchecked")
	public K copy(K target, Object source, String[] excludes) {
		return (K) BeanUtil.copyMatch(target, source, excludes);
	}

	@Override
	public K copy(K target, Object source, String[] includes, String[] excludes) {
		bind(target, source, includes);
		return copy(target, source, excludes);
	}
	
	@Override
	public K copyStrict(Class<K> clazz, Map<String, Object> source) {
		K target = getInstance(clazz);
		return copyStrict(target, source);
	}

	@Override
	public K copyStrict(Class<K> clazz, Map<String, Object> source, String[] excludes) {
		K target = getInstance(clazz);
		return copyStrict(target, source, excludes);
	}

	@Override
	public K copyStrict(Class<K> clazz, Map<String, Object> source, String[] includes, String[] excludes) {
		K target = getInstance(clazz);	
		return copyStrict(target, source, includes, excludes);
	}

	@Override
	public K copyStrict(final Class<K> clazz, final Object source) {
		K target = getInstance(clazz);	
		return copyStrict(target, source);
	}
	
	@Override
	public K copyStrict(Class<K> clazz, Object source, String[] excludes) {
		K target = getInstance(clazz);
		return copyStrict(target, source, excludes);
	}

	@Override
	public K copyStrict(Class<K> clazz, Object source, String[] includes, String[] excludes) {
		K target = getInstance(clazz);	
		return copyStrict(target, source, includes, excludes);
	}

	@Override
	@SuppressWarnings("unchecked")
	public K copyStrict(K target, Map<String, Object> source) {
		return (K) BeanUtil.copy(target, source);
	}

	@Override
	@SuppressWarnings("unchecked")
	public K copyStrict(K target, Map<String, Object> source, String[] excludes) {
		return (K) BeanUtil.copy(target, source, excludes);
	}
	
	@Override
	public K copyStrict(K target, Map<String, Object> source, String[] includes, String[] excludes) {
		bind(target, source, includes);
		return copyStrict(target, source, excludes);
	}

	@Override
	@SuppressWarnings("unchecked")
	public K copyStrict(final K target, final Object source) {
		return (K) BeanUtil.copy(target, source);
	}

	@Override
	@SuppressWarnings("unchecked")
	public K copyStrict(K target, Object source, String[] excludes) {
		return (K) BeanUtil.copy(target, source, excludes);
	}

	@Override
	public K copyStrict(K target, Object source, String[] includes, String[] excludes) {
		bind(target, source, includes);
		return copyStrict(target, source, excludes);
	}

	@Override
	public K exclude(Class<K> clazz, Map<String, Object> source, String[] excludes) {
		K target = getInstance(clazz);
		return exclude(target, source, excludes);
	}

	@Override
	public K exclude(final Class<K> clazz, final Object source, final String[] excludes) {
		K target = getInstance(clazz);
		return exclude(target, source, excludes);
	}

	@Override
	@SuppressWarnings("unchecked")
	public K exclude(K target, Map<String, Object> source, String[] excludes) {
		return (K)BeanUtil.copy(target, source, excludes);
	}

	@Override
	@SuppressWarnings("unchecked")
	public K exclude(final K target, final Object source, final String[] excludes) {
		return (K)BeanUtil.copy(target, source, excludes);
	}

	private K getInstance(Class<K> clazz) {
		K target = null;
		try {
			target = clazz.newInstance();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return target;
	}

	@Override
	public K include(Class<K> clazz, Map<String, Object> source, String[] includes) {
		K target = getInstance(clazz);
		return include(target, source, includes);
	}

	@Override
	public K include(Class<K> clazz, Object source, String[] includes) {
		K target = getInstance(clazz);
		return include(target, source, includes);
	}

	@Override
	@SuppressWarnings("unchecked")
	public K include(K target, Map<String, Object> source, String[] includes) {
		return (K)BeanUtil.copyInclude(target, source, includes);
	}

	@Override
	@SuppressWarnings("unchecked")
	public K include(final K target, final Object source, final String[] includes) {
		return (K)BeanUtil.copyInclude(target, source, includes);
	}

	public String[] inversePairProperties(final String[] pairProperties) {
		String[] inversePairProperties = new String[pairProperties.length];
		for (int index = 0; index < pairProperties.length; index++) {
			String pairProperty = pairProperties[index];
			if (pairProperty.lastIndexOf(':') != -1) {
				String[] keyValue = pairProperty.split(":");
				String inversePairProperty = keyValue[1] + ":" + keyValue[0];
				inversePairProperties[index] = inversePairProperty;
			} else {
				inversePairProperties[index] = pairProperty;
			}
		}
		return inversePairProperties;
	}
	
	private Map<String, String> putProperty(final Map<String, String> properties, String pairProperty){
		if(pairProperty.lastIndexOf(':') != -1){
			String[] keyValue = pairProperty.split(":");
			properties.put(keyValue[0], keyValue[1]);
		}else{
			properties.put(pairProperty, pairProperty);
		}
		return properties;
	}

}
