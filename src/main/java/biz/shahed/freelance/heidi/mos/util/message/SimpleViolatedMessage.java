package biz.shahed.freelance.heidi.mos.util.message;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SimpleViolatedMessage implements Serializable, ViolatedMessage {

	private static final long serialVersionUID = 5775128546116664541L;

	private boolean violated;
	private List<String> messages = new ArrayList<String>();

	public SimpleViolatedMessage() {
		// Empty
	}

	public SimpleViolatedMessage(List<String> messages) {
		this.violated = messages != null && !messages.isEmpty();
		this.messages = messages;
	}

	@Override
	public boolean isViolated() {
		return violated;
	}

	public void setViolated(boolean violated) {
		this.violated = violated;
	}

	@Override
	public List<String> getMessages() {
		return messages;
	}

	public void setMessages(List<String> messages) {
		this.messages = messages;
	}

	public void addMessage(String message) {
		if (this.messages != null) {
			messages.add(message);
		}
	}

	@Override
	public boolean isValid() {
		return !this.isViolated();
	}
	
	@Override
	public boolean notViolated() {
		return !this.isViolated();
	}
	
	@Override
	public String getMessage() {
		StringBuilder builder = new StringBuilder();
		if (messages != null) {
			String delim = "";
			for (String message : messages) {
				builder.append(delim).append(message);
				delim = ", ";
			}
		}
		return builder.toString();
	}

}
