package biz.shahed.freelance.heidi.mos.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import freemarker.cache.ClassTemplateLoader;
import freemarker.cache.TemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.ObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;

/**
 * @author Shahed Hossain <a
 *         href="mailto:eng.shahed@gmail.com">eng.shahed@gmail.com</a>
 * @version 1.0.1.RC1
 * @since January 28, 2013
 */
public class FreeMarkerUtil {

	private static Logger log = LoggerFactory.getLogger(FreeMarkerUtil.class);

	/**
	 * FreeMarker Configuration goes Here
	 * 
	 * @return <code>{@link freemarker.template.Configuration}</code>
	 * @throws IOException
	 */
	private static Configuration getConfig() throws IOException {
		Configuration cfg = new Configuration();
		cfg.setObjectWrapper(ObjectWrapper.BEANS_WRAPPER);
		return cfg;
	}

	/**
	 * FreeMarker Cache Configuration goes Here
	 * 
	 * @param clazz
	 * @param location
	 * @return <code>{@link freemarker.template.Configuration}</code>
	 */
	public static Configuration getConfig(Class<?> clazz, String location) {
		Configuration cfg = null;
		try {
			cfg = getConfig();
			TemplateLoader loader = new ClassTemplateLoader(clazz, location);
			cfg.setTemplateLoader(loader);
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}

		return cfg;
	}
	
	private static Map<String, Object> getParams() {
		Map<String, Object> params = new HashMap<String, Object>();
		return params;
	}

	private static Map<String, Object> getRoot() {
		Map<String, Object> root = new HashMap<String, Object>();
		return root;
	}

	private static Map<String, Object> getRoot(Map<String, Object> params) {
		Map<String, Object> root = getRoot();
		if(params != null && !params.isEmpty()){
			for (String key : params.keySet()) {
				if (!root.containsKey(key)) {
					Object value = params.get(key);
					root.put(key, value);
				}
			}
		}
		return root;
	}

	private static ByteArrayOutputStream getStream(Template template,
			Map<String, Object> params) throws TemplateException, IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Map<String, Object> root = getRoot(params);
		Writer writer = new OutputStreamWriter(out);
		template.process(root, writer);
		writer.flush();
		writer.close();
		return out;
	}

	private static Template getTemplate(String tpl) throws IOException {
		return getTemplate(tpl, "tplname");
	}

	private static Template getTemplate(String tpl, String tplname)
			throws IOException {
		Configuration cfg = new Configuration();
		StringReader reader = new StringReader(tpl);
		Template template = new Template(tplname, reader, cfg);
		return template;
	}

	public static String parse(Configuration cfg, String templateFile) {
		String output = "";
		try {
			if (cfg != null) {
				Template template = cfg.getTemplate(templateFile);
				output = parse(template);
			}
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}
		return output;
	}

	public static String parse(Configuration cfg, String templateFile,
			Map<String, Object> params) {
		String output = "";
		try {
			if (cfg != null) {
				Template template = cfg.getTemplate(templateFile);
				output = parse(template, params);
			}
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}
		return output;
	}

	public static String parse(String tpl) {
		String output = "";
		try {
			Template template = getTemplate(tpl);
			output = parse(template);
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}
		return output;
	}

	public static String parse(String tpl, Map<String, Object> params) {
		String output = "";
		try {
			Template template = getTemplate(tpl);
			output = parse(template, params);
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}
		return output;
	}

	public static String parse(String tpl, String tplname) {
		String output = "";
		try {
			Template template = getTemplate(tpl, tplname);
			output = parse(template);
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}
		return output;
	}

	public static String parse(String tpl, String tplname,
			Map<String, Object> params) {
		String output = "";
		try {
			Template template = getTemplate(tpl, tplname);
			output = parse(template, params);
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}
		return output;
	}

	public static String parse(Template template) {
		Map<String, Object> params = getParams();
		return parse(template, params);
	}

	public static String parse(Template template, Map<String, Object> params) {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		try {
			stream = getStream(template, params);
		} catch (TemplateException e) {
			log.error(e.getMessage(), e);
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}
		byte[] bytes = stream.toByteArray();
		String output = new String(bytes);
		return output;
	}

	public static String parseHtml(Configuration cfg, String templateFile) {
		return parse(cfg, templateFile);
	}

	public static String parseHtml(Configuration cfg, String templateFile,
			Map<String, Object> params) {
		return parse(cfg, templateFile, params);
	}

	public static String parseHtml(String tpl) {
		return parse(tpl);
	}

	public static String parseHtml(String tpl, Map<String, Object> params) {
		return parse(tpl, params);
	}

	public static String parseHtml(String tpl, String tplname) {
		return parse(tpl, tplname);
	}

	public static String parseHtml(String tpl, String tplname,
			Map<String, Object> params) {
		return parse(tpl, tplname, params);
	}

	public static String parseJs(Configuration cfg, String templateFile) {
		return parse(cfg, templateFile);
	}

	public static String parseJs(Configuration cfg, String templateFile,
			Map<String, Object> params) {
		return parse(cfg, templateFile, params);
	}

	public static String parseJs(String tpl) {
		return parse(tpl);
	}

	public static String parseJs(String tpl, Map<String, Object> params) {
		return parse(tpl, params);
	}

	public static String parseJs(String tpl, String tplname)
			throws TemplateException, IOException {
		return parse(tpl, tplname);
	}

	public static String parseJs(String tpl, String tplname,
			Map<String, Object> params) {
		return parse(tpl, tplname, params);
	}

	public static String parseJson(Configuration cfg, String templateFile) {
		return parse(cfg, templateFile);
	}

	public static String parseJson(Configuration cfg, String templateFile,
			Map<String, Object> params) {
		return parse(cfg, templateFile, params);
	}

	public static String parseJson(String tpl) {
		return parse(tpl);
	}

	public static String parseJson(String tpl, Map<String, Object> params) {
		return parse(tpl, params);
	}

	public static String parseJson(String tpl, String tplname) {
		return parse(tpl, tplname);
	}

	public static String parseJson(String tpl, String tplname,
			Map<String, Object> params) {
		return parse(tpl, tplname, params);
	}

	public static String parseXml(Configuration cfg, String templateFile) {
		return parse(cfg, templateFile);
	}

	public static String parseXml(Configuration cfg, String templateFile,
			Map<String, Object> params) {
		return parse(cfg, templateFile, params);
	}

	public static String parseXml(String tpl) {
		return parse(tpl);
	}

	public static String parseXml(String tpl, Map<String, Object> params) {
		return parse(tpl, params);
	}

	public static String parseXml(String tpl, String tplname)
			throws TemplateException, IOException {
		return parse(tpl, tplname);
	}

	public static String parseXml(String tpl, String tplname,
			Map<String, Object> params) {
		return parse(tpl, tplname, params);
	}

}
