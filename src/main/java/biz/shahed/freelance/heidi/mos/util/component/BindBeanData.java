package biz.shahed.freelance.heidi.mos.util.component;

import java.util.Map;

public interface BindBeanData<K> {
	
	K bind(final Class<K> clazz, final Map<String, Object> source, final Map<String, String> properties);
	
	K bind(final Class<K> clazz, final Map<String, Object> source, final String[] pairProperties);	

	K bind(final Class<K> clazz, final Object source, final Map<String, String> properties);
	
	K bind(final Class<K> clazz, final Object source, final String[] pairProperties);
	
	K bind(final K target, final Map<String, Object> source, final Map<String, String> properties);
	
	K bind(final K target, final Map<String, Object> source, final String[] pairProperties);
	
	K bind(final K target, final Object source, final Map<String, String> properties);
	
	K bind(final K target, final Object source, final String[] pairProperties);
		 
	K bind(final Object source, final Map<String, String> properties);
		
	K cloneBean(final K bean);

	K copy(final Class<K> clazz, final Map<String, Object> source);
	
	K copy(final Class<K> clazz, final Map<String, Object> source, final String[] excludes);
	
	K copy(final Class<K> clazz, final Map<String, Object> source, final String[] includes, final String[] excludes);

	K copy(final Class<K> clazz, final Object source);
		
	K copy(final Class<K> clazz, final Object source, final String[] excludes);
	
	K copy(final Class<K> clazz, final Object source, final String[] includes, final String[] excludes);
	
	K copy(final K target, final Map<String, Object> source);

	K copy(final K target, final Map<String, Object> source, final String[] excludes);
	
	K copy(final K target, final Map<String, Object> source, final String[] includes, final String[] excludes);
		
	K copy(final K target, final Object source);
	
	K copy(final K target, final Object source, final String[] excludes);
	
	K copy(final K target, final Object source, final String[] includes, final String[] excludes);
		
	K copyStrict(final Class<K> clazz, final Map<String, Object> source);
	
	K copyStrict(final Class<K> clazz, final Map<String, Object> source, final String[] excludes);
	
	K copyStrict(final Class<K> clazz, final Map<String, Object> source, final String[] includes, final String[] excludes);

	K copyStrict(final Class<K> clazz, final Object source);
		
	K copyStrict(final Class<K> clazz, final Object source, final String[] excludes);
	
	K copyStrict(final Class<K> clazz, final Object source, final String[] includes, final String[] excludes);
	
	K copyStrict(final K target, final Map<String, Object> source);

	K copyStrict(final K target, final Map<String, Object> source, final String[] excludes);
	
	K copyStrict(final K target, final Map<String, Object> source, final String[] includes, final String[] excludes);
		
	K copyStrict(final K target, final Object source);
	
	K copyStrict(final K target, final Object source, final String[] excludes);
	
	K copyStrict(final K target, final Object source, final String[] includes, final String[] excludes);
		
	K exclude(final Class<K> clazz, final Map<String, Object> source, final String[] excludes);
	
	K exclude(final Class<K> clazz, final Object source, final String[] excludes);

	K exclude(final K target, final Map<String, Object> source, final String[] excludes);
		
	K exclude(final K target, final Object source, final String[] excludes);
		
	K include(final Class<K> clazz, final Map<String, Object> source, final String[] includes);
	
	K include(final Class<K> clazz, final Object source, final String[] includes);

	K include(final K target, final Map<String, Object> source, final String[] includes);
	
	K include(final K target, final Object source, final String[] includes);
	
	String[] inversePairProperties(final String[] pairProperties);
		
}
