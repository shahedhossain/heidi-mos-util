package biz.shahed.freelance.heidi.mos.util;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

public class AjaxUtil {
	
	@SuppressWarnings("rawtypes")
	public static boolean isAjaxRequest(HttpServletRequest request){
		Enumeration headers = request.getHeaderNames();
		while (headers.hasMoreElements()) {
			String xRequestWith = (String) headers.nextElement();
			if(xRequestWith.equalsIgnoreCase("X-Requested-With")){
				return true;
			}
		}
		return false;
	}
	
}
