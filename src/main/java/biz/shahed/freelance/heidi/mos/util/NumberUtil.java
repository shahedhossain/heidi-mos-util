package biz.shahed.freelance.heidi.mos.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NumberUtil {

	private static final Logger log = LoggerFactory.getLogger(NumberUtil.class);

	public static int parseInt(String number) {
		int value = 0;
		if (isInt(number)) {
			try {
				value = Integer.parseInt(number);
			} catch (NumberFormatException e) {
				log.error(e.getMessage(), e);
			}
		}
		return value;
	}

	public static boolean isInt(String number) {
		boolean intFlag = false;
		if(number != null && !number.isEmpty()) {
			intFlag = number.trim().matches("[\\d]+");
		}
		return intFlag;
	}
}
