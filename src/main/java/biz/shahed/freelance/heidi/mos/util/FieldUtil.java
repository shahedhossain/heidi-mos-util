package biz.shahed.freelance.heidi.mos.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import javax.persistence.Id;

public class FieldUtil {

	public static String getGetterName(Field field){
		String prefix = isBoolean(field) ? MethodUtil.BOOL_GETTER_PREFIX : MethodUtil.GETTER_PREFIX;
		String suffix = StringUtil.initCap(field.getName());
		return prefix + suffix;
	}

	public static Field getIdField(final Object object) {
		Class<?> clazz = object.getClass();
		return ClassUtil.getIdField(clazz);
	}

	public static String getSetterName(Field field){
		String prefix = MethodUtil.SETTER_PREFIX;
		String suffix = StringUtil.initCap(field.getName());
		return prefix + suffix;
	}

	public static boolean isBoolean(Field field) {
		return ClassUtil.isBoolean(field.getType());
	}

	public static boolean isBoolean(Field field, boolean isPrimitive) {
		return ClassUtil.isBoolean(field.getType(), isPrimitive);
	}

	public static boolean isBooleanPrimitive(Field field) {
		return ClassUtil.isBooleanPrimitive(field.getType());
	}

	public static boolean isBooleanWrapper(Field field) {
		return ClassUtil.isBooleanWrapper(field.getType());
	}
	
	public static boolean isByte(Field field) {
		return ClassUtil.isByte(field.getType());
	}

	public static boolean isByte(Field field, boolean isPrimitive) {
		return ClassUtil.isByte(field.getType(), isPrimitive);
	}

	public static boolean isBytePrimitive(Field field) {
		return ClassUtil.isBytePrimitive(field.getType());
	}

	public static boolean isByteWrapper(Field field) {
		return ClassUtil.isByteWrapper(field.getType());
	}

	public static boolean isCharacter(Field field) {
		return ClassUtil.isCharacter(field.getType());
	}

	public static boolean isCharacter(Field field, boolean isPrimitive) {
		return ClassUtil.isCharacter(field.getType(), isPrimitive);
	}

	public static boolean isCharacterPrimitive(Field field) {
		return ClassUtil.isCharacterPrimitive(field.getType());
	}

	public static boolean isCharacterWrapper(Field field) {
		return ClassUtil.isCharacterWrapper(field.getType());
	}

	public static boolean isCollection(Field field) {
		return ClassUtil.isCollection(field.getType());
	}
	
	public static boolean isDouble(Field field) {
		return ClassUtil.isDouble(field.getType());
	}
	
	public static boolean isDouble(Field field, boolean isPrimitive) {
		return ClassUtil.isDouble(field.getType(), isPrimitive);
	}

	public static boolean isDoublePrimitive(Field field) {
		return ClassUtil.isDoublePrimitive(field.getType());
	}

	public static boolean isDoubleWrapper(Field field) {
		return ClassUtil.isDoubleWrapper(field.getType());
	}

	public static boolean isFinal(Field field){
		return Modifier.isFinal(field.getModifiers());
	}

	public static boolean isFloat(Field field) {
		return ClassUtil.isFloat(field.getType());
	}

	public static boolean isFloat(Field field, boolean isPrimitive) {
		return ClassUtil.isFloat(field.getType(), isPrimitive);
	}

	public static boolean isFloatPrimitive(Field field) {
		return ClassUtil.isFloatPrimitive(field.getType());
	}

	public static boolean isFloatWrapper(Field field) {
		return ClassUtil.isFloatWrapper(field.getType());
	}

	public static boolean isIdField(Field field){
		for (Annotation annotation : field.getAnnotations()) {
			if (annotation instanceof Id) {
				return true;
			}
		}
		return false;
	}

	public static boolean isInteger(Field field) {
		return ClassUtil.isInteger(field.getType());
	}

	public static boolean isInteger(Field field, boolean isPrimitive) {
		return ClassUtil.isInteger(field.getType(), isPrimitive);
	}

	public static boolean isIntegerPrimitive(Field field) {
		return ClassUtil.isIntegerPrimitive(field.getType());
	}

	public static boolean isIntegerWrapper(Field field) {
		return ClassUtil.isIntegerWrapper(field.getType());
	}

	public static boolean isList(Field field) {
		return ClassUtil.isList(field.getType());
	}

	public static boolean isLong(Field field) {
		return ClassUtil.isLong(field.getType());
	}
	
	public static boolean isLong(Field field, boolean isPrimitive) {
		return ClassUtil.isLong(field.getType(), isPrimitive);
	}
	
	public static boolean isLongPrimitive(Field field) {
		return ClassUtil.isLongPrimitive(field.getType());
	}

	public static boolean isLongWrapper(Field field) {
		return ClassUtil.isLongWrapper(field.getType());
	}
	
	public static boolean isMap(Field field) {
		return ClassUtil.isMap(field.getType());
	}
	
	public static boolean isPrimitive(Field field) {
		return ClassUtil.isPrimitive(field.getType());
	}
	
	public static boolean isPrivate(Field field){
		return Modifier.isPrivate(field.getModifiers());
	}

	public static boolean isProtected(Field field){
		return Modifier.isProtected(field.getModifiers());
	}

	public static boolean isPublic(Field field){
		return Modifier.isPublic(field.getModifiers());
	}
	
	public static boolean isSet(Field field) {
		return ClassUtil.isSet(field.getType());
	}
	
	public static boolean isShort(Field field) {
		return ClassUtil.isShort(field.getType());
	}
	
	public static boolean isShort(Field field, boolean isPrimitive) {
		return ClassUtil.isShort(field.getType(), isPrimitive);
	}
	
	public static boolean isShortPrimitive(Field field) {
		return ClassUtil.isShortPrimitive(field.getType());
	}
	
	public static boolean isShortWrapper(Field field) {
		return ClassUtil.isShortWrapper(field.getType());
	}
	
	public static boolean isStatic(Field field){
		return Modifier.isStatic(field.getModifiers());
	}
	
	public static boolean isString(Field field) {
		return ClassUtil.isString(field.getType());
	}
	
	public static boolean isWrapper(Field field) {
		return ClassUtil.isWrapper(field.getType());
	}
	
}
