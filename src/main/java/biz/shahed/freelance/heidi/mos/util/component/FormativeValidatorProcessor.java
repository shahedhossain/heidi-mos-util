package biz.shahed.freelance.heidi.mos.util.component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidatorFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import biz.shahed.freelance.heidi.mos.util.EntityUtil;
import biz.shahed.freelance.heidi.mos.util.message.SimpleViolatedMessage;
import biz.shahed.freelance.heidi.mos.util.message.ViolatedMessage;


@Component
public class FormativeValidatorProcessor implements FormativeValidator {

	private FormativeMessage formativeMessage;

	@Autowired
	public FormativeValidatorProcessor(FormativeMessage formativeMessage) {
		this.formativeMessage = formativeMessage;
	}

	private Set<ConstraintViolation<Object>> getViolations(Object object) {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		return factory.getValidator().validate(object);
	}

	@Override
	public ViolatedMessage notEmpty(Collection<?> collection) {
		String message = "Collections must not be empty!";
		return notEmpty(collection, message);
	}

	@Override
	public ViolatedMessage notEmpty(Collection<?>... collections) {
		for (Collection<?> collection : collections) {
			ViolatedMessage violation = notEmpty(collection);
			if (violation.isViolated()) {
				return violation;
			}
		}
		return new SimpleViolatedMessage();
	}

	@Override
	public ViolatedMessage notEmpty(Collection<?> collection, String message) {
		List<String> messages = new ArrayList<String>();
		if (collection != null && !collection.isEmpty()) {
			return new SimpleViolatedMessage();
		}
		messages.add(formativeMessage.getMessage(message));
		return new SimpleViolatedMessage(messages);
	}

	@Override
	public ViolatedMessage notEmpty(String message, Collection<?>... collections) {
		for (Collection<?> collection : collections) {
			ViolatedMessage violation = notEmpty(collection, message);
			if (violation.isViolated()) {
				return violation;
			}
		}
		return new SimpleViolatedMessage();
	}

	@Override
	public ViolatedMessage notNull(Object value) {
		String message = "Null pointer exception occured!";
		return notNull(value, message);
	}

	@Override
	public ViolatedMessage notNull(Object... objects) {
		for (Object object : objects) {
			ViolatedMessage violation = notNull(object);
			if (violation.isViolated()) {
				return violation;
			}
		}
		return new SimpleViolatedMessage();
	}

	@Override
	public ViolatedMessage notNull(Object value, String message) {
		List<String> messages = new ArrayList<String>();
		if (value != null) {
			return new SimpleViolatedMessage();
		}
		messages.add(formativeMessage.getMessage(message));
		return new SimpleViolatedMessage(messages);
	}

	@Override
	public ViolatedMessage notNull(String message, Object... objects) {
		for (Object object : objects) {
			ViolatedMessage violation = notNull(object, message);
			if (violation.isViolated()) {
				return violation;
			}
		}
		return new SimpleViolatedMessage();
	}

	@Override
	public ViolatedMessage validate(Collection<?> collection) {
		Iterator<?> iterator = collection.iterator();
		while (iterator.hasNext()) {
			Object object = iterator.next();
			ViolatedMessage violation = validate(object);
			if (violation.isViolated()) {
				return violation;
			}
		}
		return new SimpleViolatedMessage();
	}

	@Override
	public ViolatedMessage validate(Object object) {
		List<String> messages = new ArrayList<String>();
		if (object != null) {
			EntityUtil.filterEntity(object);
			Set<ConstraintViolation<Object>> violations = getViolations(object);
			if (violations.isEmpty()) {
				return new SimpleViolatedMessage();
			} else {
				for (ConstraintViolation<Object> violation : violations) {
					String key = violation.getMessage();
					Object value = violation.getInvalidValue();
					messages.add(formativeMessage.getMessage(key, value));
				}
			}
		} else {
			messages.add("Null object reference passed!");
		}
		return new SimpleViolatedMessage(messages);
	}

	@Override
	public ViolatedMessage validate(Object... objects) {
		for (Object object : objects) {
			if (object != null) {
				if (object instanceof Collection<?>) {
					Collection<?> collection = (Collection<?>) object;
					ViolatedMessage violation = validate(collection);
					if (violation.isViolated()) {
						return violation;
					}
				} else {
					ViolatedMessage violation = validate(object);
					if (violation.isViolated()) {
						return violation;
					}
				}
			}
		}
		return new SimpleViolatedMessage();
	}

}
