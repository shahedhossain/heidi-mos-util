package biz.shahed.freelance.heidi.mos.util.component;

import java.util.Locale;

public interface FormativeMessage {

	public String getMessage(String key, Object[] parameters, Locale locale);

	public String getMessage(String key, Object[] parameters);

	public String getMessage(String key, Object parameter);

	public String getMessage(String key);
}
