package biz.shahed.freelance.heidi.mos.util.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.dao.SaltSource;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import biz.shahed.freelance.heidi.mos.util.AjaxUtil;

@Service
@Transactional(readOnly = false)
public class SpringSecurityServiceImpl implements SpringSecurityService {

	@Autowired 
	private SaltSource saltSource;
	
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	public String passwordEncoder(UserDetails user){
		return passwordEncoder.encodePassword(user.getPassword(), saltSource.getSalt(user));
	}
	
	public boolean isAjax(HttpServletRequest request) {
		return AjaxUtil.isAjaxRequest(request);
	}

	public String getCurrentUser() {
		if (!isLoggedIn()) {
			return null;
		}
		Object principal = getPrincipal();
		if(principal instanceof String){
			return (String) principal;
		}else if(principal instanceof User){
			UserDetails user = (UserDetails) principal;
			return user.getUsername();
		}
		return null;		
	}

	public boolean isLoggedIn() {
		Authentication authentication = getAuthentication();
		if (authentication != null && authentication.isAuthenticated()) {
			if (!authentication.getName().equals("anonymousUser")) {
				return true;
			}
		}
		return false;
	}

	public Object getPrincipal() {
		Authentication authentication = getAuthentication();
		if (authentication != null) {
			return authentication.getPrincipal();
		}
		return null;
	}

	public Authentication getAuthentication() {
		SecurityContext securityContext = getSecurityContext();
		if (securityContext != null) {
			return securityContext.getAuthentication();
		}
		return null;
	}

	public SecurityContext getSecurityContext() {
		return SecurityContextHolder.getContext();
	}

}
