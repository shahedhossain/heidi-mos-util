package biz.shahed.freelance.heidi.mos.util.javascript;

import org.mozilla.javascript.ErrorReporter;
import org.mozilla.javascript.EvaluatorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JavaScriptErrorReporter implements ErrorReporter {

	private static Logger log = LoggerFactory.getLogger(JavaScriptErrorReporter.class);
	private static final String MSG_EXP = "[line :%s, column:%s] %s";

	@Override
	public void warning(String message, String sourceName, int line,
			String lineSource, int lineOffset) {
		if (line < 0) {
			log.warn(message);
		} else {
			message = String.format(MSG_EXP, line, lineOffset, message);
			log.warn(message);
		}
	}

	@Override
	public void error(String message, String sourceName, int line,
			String lineSource, int lineOffset) {
		if (line < 0) {
			log.error(message);
		} else {
			message = String.format(MSG_EXP, line, lineOffset, message);
			log.error(message);
		}
	}

	@Override
	public EvaluatorException runtimeError(String message, String sourceName, int line, String lineSource, int lineOffset) {
		error(message, sourceName, line, lineSource, lineOffset);
		return new EvaluatorException(message);
	}
}
