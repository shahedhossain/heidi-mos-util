package biz.shahed.freelance.heidi.mos.util;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;



public class SpringUtil {
	
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(SpringUtil.class);
	public static final String EXTMVC_BEAN_NAMES = "e[0-9]{2}[B|I|Q|R|S|T]{1}[0-9]{6}ExtImpl";
	public static final String EXTMVC_BEAN_SUFFIX = "[B|I|Q|R|S|T]{1}[0-9]{6}ExtImpl";
	public static final String EXTMVC_BEAN_PREFIX = "e";

	public static Set<String> getBeanNames(ApplicationContext appctx, String beanexp) {
		Set<String> beanNames = new TreeSet<String>();
		for (String beanName : appctx.getBeanDefinitionNames()) {
			if (PatternUtil.isMatch(beanName, beanexp)) {
				beanNames.add(beanName);
			}
		}
		return beanNames;
	}

	public static Set<String> getAllExtmvcNames(ApplicationContext appctx) {
		return getBeanNames(appctx, EXTMVC_BEAN_NAMES);
	}

	public static Set<String> getModularExtmvcNames(Set<String> extmvcNames, String moduleexp) {
		Set<String> modularBeanNames = new TreeSet<String>();
		for (String beanName : extmvcNames) {
			if (PatternUtil.isMatch(beanName, moduleexp)) {
				modularBeanNames.add(beanName);
			}
		}
		return modularBeanNames;
	}

	private static Set<String> getModulePrefixes(Set<String> extmvcNames) {
		Set<String> modulePrefixes = new TreeSet<String>();
		for (String beanName : extmvcNames) {
			modulePrefixes.add(beanName.substring(0, 3));
		}
		return modulePrefixes;
	}

	private static String getExpression(String modulePrefix) {
		return modulePrefix + EXTMVC_BEAN_SUFFIX;
	}

	private static String getModuleName(String modulePrefix) {
		String prefix = "M";
		String middle = modulePrefix.substring(1, 3);
		String suffix = "001";
		return prefix + middle + suffix;
	}

	public static Map<String, String> getExpression(Set<String> extmvcNames) {
		Map<String, String> exprssionMap = new TreeMap<String, String>();
		Set<String> modulePrefixes = getModulePrefixes(extmvcNames);		
		for (String modulePrefix : modulePrefixes) {
			String module = getModuleName(modulePrefix);
			String regexp = getExpression(modulePrefix);
			exprssionMap.put(module, regexp);
		}
		return exprssionMap;
	}

}