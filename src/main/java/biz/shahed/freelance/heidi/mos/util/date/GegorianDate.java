package biz.shahed.freelance.heidi.mos.util.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import biz.shahed.freelance.heidi.mos.util.DateUtil;
import biz.shahed.freelance.heidi.mos.util.StringUtil;

public class GegorianDate {

	private static Logger logger = LoggerFactory.getLogger(GegorianDate.class.getName());

	public GegorianDate(Date arabicDate) {
		
		int year = parseInt(arabicDate, YYYY);
		int month = parseInt(arabicDate, MM);
		int date = parseInt(arabicDate, DD);
		
		gegorianDate = englishDate(year, month, date);
	}

	private Date englishDate(int year, int month, int date) {
		
		double jd;
		double l;
		double n;
		double i;
		double j;

		jd = intval((11 * year + 3) / 30);
		jd += 354 * year + 30 * month;
		jd -= intval((month - 1) / 2);
		jd += date + 1948440 - 385;

		if (jd > 2299160) {
			
			l = jd + 68569;
			n = intval((4 * l) / 146097);
			l = l - intval((146097 * n + 3) / 4);
			i = intval((4000 * (l + 1)) / 1461001);
			l = l - intval((1461 * i) / 4) + 31;
			j = intval((80 * l) / 2447);
			date = intval(l - (intval((2447 * j) / 80) + 1));
			l = intval(j / 11);
			month = intval(j + 2 - 12 * l);
			year = intval(100 * (n - 49) + i + l);
			
		} else {
			
			j = jd + 1402;
			int k = intval((j - 1) / 1461);
			l = j - 1461 * k;
			n = intval((l - 1) / 365) - intval(l / 1461);
			i = l - 365 * n + 30;
			j = intval((80 * i) / 2447);
			date = intval(i - (intval((2447 * j) / 80) + 1));
			i = intval(j / 11);
			month = intval(j + 2 - 12 * i);
			year = intval(4 * k + n + i - 4716);
			
		}

		Date endate = null;
		
		try {
			
			String source = "" + lpad(date) + "/" + lpad(month) + "/" + year;
			endate = (new SimpleDateFormat(DD_MM_YYYY)).parse(source);
			
		} catch (ParseException e) {
			logger.error(e.getMessage(), e);
		}

		return endate;
	}

	public int parseInt(Date date, String pattern) {
		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		return Integer.parseInt(formatter.format(date));
	}

	private String lpad(int input) {
		return StringUtil.lpad(input + "", 2, '0');
	}

	private int intval(double number) {
		return (int) number;
	}

	public Date getGegorianDate() {
		return gegorianDate;
	}

	private Date gegorianDate;
	
	private final String YYYY = "yyyy";
	private final String MM = "MM";
	private final String DD = "dd";
	public static final String DD_MM_YYYY = DateUtil.DD_MM_YYYY;
}
