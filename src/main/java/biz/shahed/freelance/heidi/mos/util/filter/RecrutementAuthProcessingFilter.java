package biz.shahed.freelance.heidi.mos.util.filter;

import java.io.IOException;
import java.io.Writer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

public class RecrutementAuthProcessingFilter extends UsernamePasswordAuthenticationFilter {

	public static final String METADATA     = "{\"implicitIncludes\":true, \"messageProperty\":\"message\", \"successProperty\":\"success\"}";
	public static final String MESSAGE_TPL	= "{\"metaData\":%s, \"success\":%s, \"message\":\"%s\"}";

	
	@Override
	@SuppressWarnings("deprecation")
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, Authentication authResult) throws IOException, ServletException {
		SavedRequestAwareAuthenticationSuccessHandler srh = new SavedRequestAwareAuthenticationSuccessHandler();
		this.setAuthenticationSuccessHandler(srh);
		srh.setRedirectStrategy(new RedirectStrategy() {
			@Override
			public void sendRedirect(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, String s) throws IOException {
			}
		});
		super.successfulAuthentication(request, response, authResult);
		HttpServletResponseWrapper responseWrapper = new HttpServletResponseWrapper(response);
		String message = String.format("Welcome %s", authResult.getName());
		Writer out = responseWrapper.getWriter();
		out.write(getJsonResponse(true, message));
		out.close();
	}

	@Override
	protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException, ServletException {
		super.unsuccessfulAuthentication(request, response, failed);
		HttpServletResponseWrapper responseWrapper = new HttpServletResponseWrapper(response);
		Writer out = responseWrapper.getWriter();
		out.write(getJsonResponse(false, failed.getMessage()));
		out.close();
	}
	
	public String getJsonResponse(boolean success, String message){
		return String.format(MESSAGE_TPL, METADATA, success, message);		
	}
}
